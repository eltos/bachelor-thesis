from quantities import *
from quantities.constants import *
import numpy as np
import matplotlib
from matplotlib import pyplot

matplotlib.rc('pdf', fonttype=42)


# Definitions
#############

# Motion equation
def norm(v):
    return (v.T[0]**2+v.T[1]**2)**0.5
    
def pos(epos, phi):
    # offset
    x0 = epos.T[0]
    y0 = epos.T[1]
    
    # larmor
    x1 = rl*(cos(phi)+1)
    y1 = rl*(sin(phi)+0)
    
    # galloping
    r = xi*norm(epos-cog)
    delta = (epos-cog).T
    rho = -(rho0 + arctan2(delta[0], delta[1]))
    x2 = r*(cos(phi+rho)+cos(rho))
    y2 = r*(sin(phi+rho)+sin(rho))
    
    # quadrupole motion
    r = nu*norm(epos-cog)
    delta = (epos-cog).T
    theta = theta0 + arctan2(delta[0], delta[1])
    x3 = r*(cos(phi+theta)+cos(theta))
    y3 = r*(sin(phi+theta)+sin(theta))
    
    return x0+x1+x2+x3, y0+y1+y2+y3





# Plot
#######
def plot(plottitle, filename=None):
    fig, ax = pyplot.subplots(figsize=(5,5))

    # electron trajectory
    for r in R:
        if r > 0:
            EPOS = np.array((r*cos(A),r*sin(A))).T*mm
        else:
            EPOS = np.array([[0,0]])*mm
        for epos in EPOS:
            PHI = phi0 + 2*pi*np.linspace(0,1)
            X,Y = pos(epos, PHI)
            ax.plot(X,Y,':',c='grey')
            ax.plot(*epos,'k+')

    # single electrons and envelope
    superposition = ([],[])
    for t in T:
        phi = phi0 + t*2*pi

        for r in R:
            # single electrons
            if r > 0:
                EPOS = np.array((r*cos(A),r*sin(A))).T*mm
            else:
                EPOS = np.array([[0,0]])*mm
            X,Y = pos(EPOS, phi)
            ax.plot(X,Y,'o',c='tab:blue',alpha=1-t,markersize=4)

            if (2*t)%1 == 0:
                # beam envelope
                B = np.linspace(0, 2*pi, 100)
                EPOS = np.array((r*cos(B),r*sin(B))).T*mm
                X,Y = pos(EPOS, phi)
                ax.plot(X,Y,'-',c='tab:blue',alpha=1-t)


    ax.set_title(plottitle, fontsize=22)
    ax.grid()
    ax.set_xlabel('$x$ / mm')
    ax.set_ylabel('$y$ / mm')
    ax.set_xlim(limit)
    ax.set_ylim(limit)
    ax.set_aspect('equal', adjustable='box')

    #fig.tight_layout()
    if filename:
        fig.savefig(filename, dpi=300)
    else:
        pyplot.show()
        


# Beam center of galloping motion
cog = np.array((0,0)) * mm
# Larmor
_rl = 300 * um
# Galloping
_xi = 150 * um/mm
# Quadrupole motion
_nu = 150 * um/mm

# phases
phi0 = 2*pi/2
rho0 = pi/2
theta0 = pi/2

# Plot limit
limit = (-2.7,2.7)
# Radial-azimutal grid
R = np.arange(0,3) * mm
A = np.linspace(0, 2*pi, 8+1)
T = np.linspace(0,1,10+1)[:-1]



# Plot
#######
rl, xi, nu = 0*_rl, 0*_xi, 0*_nu
plot('Idealfall', 'electron_motion_0.pdf')

rl, xi, nu = 1*_rl, 0*_xi, 0*_nu
plot('Larmor Rotation', 'electron_motion_1.pdf')

rl, xi, nu = 0*_rl, 1*_xi, 0*_nu
plot('Galloping', 'electron_motion_2.pdf')

rl, xi, nu = 0*_rl, 0*_xi, 1*_nu
plot('Quadrupol-Bewegung', 'electron_motion_3.pdf')

rl, xi, nu = 1*_rl, 1*_xi, 1*_nu
plot('Überlagerte Bewegung', 'electron_motion_all.pdf')

















