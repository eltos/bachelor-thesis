import matplotlib
from matplotlib import pyplot as plt
from matplotlib.patches import ConnectionPatch
from matplotlib.colors import LinearSegmentedColormap
import numpy as np

matplotlib.rc('pdf', fonttype=42)




def gaussian(x, mu, sig):
	return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))
def gaussian2D(x, y, sigma=np.matrix([[1,0],[0,1]]), x0=0, y0=0, A=1):
	"""Gaussian function
	"""
	#x = np.matrix((x-x0, y-y0)).transpose()
	#return A*np.exp(-1/2*(x.transpose()*sigma**-1*x))
	s = sigma**-1
	x = x-x0
	y = y-y0
	return A*np.exp(-1/2*(s[0,0]*x**2+(s[0,1]+s[1,0])*x*y+s[1,1]*y**2))
	


# Parameters
sigma = np.matrix([[1.5,0.5],[0.5,1]])
#sigma = np.matrix([[1,1],[1,2]])
#sigma = np.matrix([[1,0],[0,10]])
emittance = np.sqrt(np.linalg.det(sigma))
print('emittance = ', emittance)
sigmax = np.sqrt(sigma[0,0])
sigmaxp = np.sqrt(sigma[1,1])
r12 = np.sqrt(sigma[0,1]*sigma[1,0]/(sigma[0,0]*sigma[1,1]))



# Phase space dimensions
scale = 2.5*max(sigmax, sigmaxp)
Xp = X = np.linspace(-scale, scale, 100)

# Density function
rho = lambda x, xp: gaussian2D(x, xp, sigma)
rhoSigma = np.e**-0.5*rho(0,0)
rho2Sigma = np.e**-2*rho(0,0)
rho3Sigma = np.e**-4.5*rho(0,0)


intensity = lambda x: sum([rho(x, xp) for xp in Xp])





# Plots
fig, (prax, shax, phax)  = plt.subplots(1, 3, sharey=True,
		#gridspec_kw = {'width_ratios':[3, 7], 'wspace':0.1}, figsize=(10, 7))
		gridspec_kw = {'width_ratios':[2.5, 0.3, 5], 'wspace':0.05}, figsize=(8, 5))


# Phasespace plot
CX,CXp = np.meshgrid(X, Xp)
CI = rho(CX, CXp)
cmap = 'Reds'
cmap = LinearSegmentedColormap.from_list("", ["white","red","darkred"])
phax.pcolormesh(CXp, CX, CI, cmap=cmap, shading='gouraud')
phaxc = phax.contour(CXp, CX, CI, [rhoSigma], colors='k')
phax.clabel(phaxc, inline=True, fmt={phaxc.levels[0]:r'$1\sigma$'})

phax.axhline(y=0, color='k', linewidth=0.5)
phax.axvline(x=0, color='k', linewidth=0.5)
phax.set_title('Phasenraum')
phax.set_xlabel('Divergenz x\'')
phax.set_ylabel('Position x')
phax.yaxis.set_label_position("right")
phax.get_xaxis().set_ticks([])
phax.get_yaxis().set_ticks([])
phax.set_aspect('equal', adjustable='box-forced')
#for border in phax.spines:
#	phax.spines[border].set_visible(False)


# Shape plot
CX, CY = np.meshgrid(X, [0,1])
CI = intensity(CX)
shax.pcolormesh(CY, CX, CI, cmap=cmap, shading='gouraud')

shax.set_xlabel('z')
#shax.set_ylabel('Position x')
#shax.yaxis.set_label_position("right")
shax.get_xaxis().set_ticks([])
shax.get_yaxis().set_ticks([])


# Profile plot
I = intensity(X)
prax.plot(I, X, 'r')

prax.axhline(y=0, color='k', linewidth=0.5)
prax.axvline(x=0, color='k', linewidth=0.5)
prax.set_title('Strahlprofil')
prax.set_xlabel('Intensität')
prax.get_xaxis().set_ticks([])
prax.get_yaxis().set_ticks([])
prax.invert_xaxis()
#for border in prax.spines:
#	prax.spines[border].set_visible(False)
#prax.spines['top'].set_visible(False)
#prax.spines['left'].set_visible(False)
	
	
# annotations
def multi_axis_line(ax0, xy0, ax1, xy1, **kwargs):
	style = {'facecolor':'black', 'arrowstyle':'-', 'linewidth':0.5}
	style.update(kwargs)
	ax0.figure.axes[-1].annotate('', xy=xy0, xytext=xy1, xycoords=ax0.transData, textcoords=ax1.transData,
					arrowprops=style)

multi_axis_line(phax, (r12*sigmaxp, sigmax), prax, (intensity(sigmax), sigmax), linestyle='-', alpha=0.5)
multi_axis_line(phax, (-r12*sigmaxp, -sigmax), prax, (intensity(-sigmax), -sigmax), linestyle='-', alpha=0.5)

phax.plot((-sigmaxp, 0), (-r12*sigmax, -r12*sigmax), 'k-', linewidth=0.5)
phax.text(-sigmaxp/2, -r12*sigmax, r'$\sqrt{\sigma_{22}}$', horizontalalignment='center', verticalalignment='bottom')
phax.plot((r12*sigmaxp, r12*sigmaxp), (0, sigmax), 'k-', linewidth=0.5)
phax.text(r12*sigmaxp, sigmax/2, r'$\sqrt{\sigma_{11}}$', horizontalalignment='right', verticalalignment='center', rotation=90)
prax.plot((0.5*intensity(sigmax), 0.5*intensity(sigmax)), (0, sigmax), 'k-', linewidth=0.5)
prax.text(0.5*intensity(sigmax), sigmax/2, r'$\sigma_x$', horizontalalignment='right', verticalalignment='center', rotation=90)

#phax.annotate('Phasenellipse', xy=())


phax.margins(x=0, y=0)
prax.set_xlim([prax.get_xlim()[0], 0])
#fig.tight_layout()

plt.savefig('phasespace.pdf')
print('saved to phasespace.pdf')
plt.show()



