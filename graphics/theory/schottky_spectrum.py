import numpy as np
import matplotlib
from matplotlib import pyplot as plt

matplotlib.rc('pdf', fonttype=42)

def gaussian(x, mu, sig):
	return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))
	


f = 25
df = 1
N = 7

i = 1
X = np.linspace(0, (N+.5)*f, 10000)
Ys = np.zeros(len(X))
YY = []
while i*(f-3*df) < max(X):
	YY.append(gaussian(X, i*f, i*df)*1/i)
	Ys += YY[-1]
	i += 1
	
Ys[0] = 10 # DC
	
	
plt.figure(figsize=[6,2.5])
for Y in YY:
	plt.plot(X,Y,'tab:orange',linewidth=0.5)
plt.plot(X,Ys,'tab:red')
#plt.plot(X[1:], f/X[1:], ':', c='tab:orange')

hwhm = (2*np.log(2))**0.5*df
for i in range(1, 3):
	plt.plot(i*np.array((f-hwhm, f+hwhm)), (0.5/i, 0.5/i), 'k-', linewidth=1)
	tx = i*f+4*hwhm
	ty = 0.5/i + 0.1
	plt.plot((i*f, tx), (0.5/i, ty), 'k-', linewidth=0.5)
	plt.text(tx, ty, '$\Delta f$' if i==1 else r'$%i \Delta f$'%i, horizontalalignment='left', verticalalignment='center')
	

plt.title('Longitudinales Schottky-Spektrum')
plt.xlabel('$Frequenz$')
plt.ylabel('$\sum-Signal$')
plt.yticks([])
plt.xticks([f*_ for _ in range(1, N+1)], ['$f$']+['$%i f$'%i for i in range(2, N+1)])
plt.xlim([f/2, max(X)])
plt.ylim([0, 1.1])
plt.tight_layout()

plt.savefig('schottky_spectrum.pdf')
print('saved to schottky_spectrum.pdf')
plt.show()