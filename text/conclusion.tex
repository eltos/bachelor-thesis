\section{Ergebnisdarstellung}

Die Kühlung durch den \zmev Elektronenkühler an \gls{cosy} wurde qualitativ und quantitativ untersucht.
In den Messreihen wurde ein Protonenstrahl (Impuls \SI{2.425}{\giga\eV\per\c}) mit einem Elektronenstrom von \SI{0.6}{\ampere} gekühlt (siehe \cref{cooling-parameter}).

Dabei zeigt sich eine mit $\tau=\SI{18.6+-0.6}{\second}$ schnelle und sehr starke longitudinale Phasenraumkompression mit Impulsbreiten von $\delta=\SI{5.4+-0.1 E-5}{}$.
In transversaler Ebene können Emittanzen von \SI{0.06}{\milli\meter\milli\radian} bei einer Zeitkonstante von $T=\SI{100}{\second}$ erreicht werden.
Sowohl Energieverlust als auch Strahlaufweitung durch ein im Protonenstrahl befindliches Wasserstoff Cluster-Jet Target können bis zu einer H\textsubscript{2}-Dichte von \SI[per-mode=reciprocal]{2E14}{\per\square\centi\meter} kompensiert werden.
Bei höheren Target-Dichten ist dies durch Kombination der Elektronenkühlung mit \acrlong{bb} und stochastischer Kühlung möglich.
\\

Der Elektronenkühler arbeitet besonders effizient bei einem kleinen Durchmesser des Protonenstrahls, was einer geringen Emittanz entspricht.
Die Kühlgeschwindigkeit in allen Ebenen kann verdoppelt werden, wenn die Emittanz zuvor mittels transversaler stochastischer Kühlung reduziert wird.
So können relative Impulsbreiten von $\delta=\SI{3.0+-0.2 e-5}{}$ mit longitudinalen Kühlzeiten von $\tau=\SI{7.7+-0.3}{\second}$ sowie transversalen Kühlzeiten von $T=\SI{69+-2}{\second}$ horizontal beziehungsweise $T=\SI{88+-2}{\second}$ vertikal erreicht werden.
Damit werden die Vorteile der beiden komplementären Kühlverfahren kombiniert (siehe \cref{comparison}).
\\

In der Kühlstrecke wurde die Oszillation der Elektronen untersucht, welche charakteristisch für die magnetische Hochenergie-Elektronenkühlung ist (siehe \cref{cooling-profile}).
Dabei kann eine effektive Reduzierung der longitudinalen Geschwindigkeitskomponente in Abhängigkeit von der Entfernung zum Zentrum des Elektronenstrahls beobachtet werden, welche hauptsächlich in horizontaler Ebene ausgeprägt ist.
Diese kann durch spiralförmige Bewegungen der Elektronen um die Magnetfeldlinien des Hauptsolenoiden teilweise erklärt werden und stellt einen Ansatzpunkt zur Verbesserung der transversalen Kühlung dar.
\\

Im Rahmen der Arbeit wurde die Datenerfassung des Elektronenkühlers sowie der Schottky-Spektren in das Kontrollsystem \gls{epics} eingebunden.
Dadurch wurde eine zentrale, einheitliche und dauerhafte Speicherung zusammen mit weiteren Daten des Beschleunigers realisiert, die eine vereinfachte Datenanalyse und -korrelation ermöglicht (siehe \cref{development}).





\section{Diskussion}
\subsection*{Einordnung}

Neben \gls{cosy} gibt es nur wenige weitere Beschleuniger, die sowohl über ein System zur stochastischen Kühlung wie auch über einen Elektronenkühler verfügen.
Die \glsfmt{\emph}{\gls{hirfl}} betreibt mit dem \glsfmt{\emph}{\gls{csre}} einen Speicherring, in dem Schwerionen im Energiebereich von \SIrange[per-mode=symbol]{350}{400}{\mega\electronvolt\per\atomicmassunit} seit 2015 stochastisch vor-gekühlt werden können, um die Kühlzeit durch die Elektronenkühlung wesentlich zu senken~\cite{Wu.2009}.
Am \gls{esr} des GSI Helmholtzzentrum für Schwerionenforschung können ebenfalls beide Kühlverfahren genutzt werden, was beispielsweise für Protonen bei \SI{400}{\mega\electronvolt} gezeigt wurde~\cite{Robach.2016}.
Am CERN arbeitet der \glsfmt{\emph}{\gls{ad}}~\cite{cern.ad} zwar mit beiden Methoden, jedoch in unterschiedlichen Energiebereichen~\cite{cern.cooling}.

Die hier dargelegten Ergebnisse sind ein experimentelles Beispiel für das Zusammenspiel beider Verfahren im Hochenergie-Bereich (siehe \cref{comparison}).
Mit Protonen bei \SI{2.6}{\giga\electronvolt} konnten hohe Kühlraten und geringe Impulsbreiten erreicht werden.
Für Experimente kann die Auflösung und Genauigkeit so durch einen scharf definierten Impuls und geringe transversale Impulskomponenten wesentlich erhöht werden.
Mit Blick auf das PANDA-Experiment am \gls{hesr} konnte gezeigt werden, dass die Anforderungen an Strahlqualität und Lebensdauer im Target-Betrieb auf diese Weise erfüllt werden können.
Zum jetzigen Zeitpunkt ist jedoch noch unklar, ob zusätzlich zur stochastischen Kühlung auch ein Elektronenkühler im \gls{hesr} zum Einsatz kommen wird.
\\

Die im Zuge der Arbeit entwickelte Software zur Datenerfassung und -auswertung (\cref{development}) ist nicht nur Basis der für diese Arbeit ausgewerteten Messungen.
Vielmehr wird sie auch für den weiteren Betrieb sowie zukünftige Experimente hilfreich sein.
Durch die Einbindung in das Kontrollsystem \gls{epics} und die damit verbundene zentrale Speicherung können Daten auch nachträglich ausgewertet und überprüft werden.
Das offene \gls{epics}-Interface bietet außerdem die Möglichkeit die Daten auch in andere Systeme einzuspeisen und in Echtzeit zu nutzen.
Hierbei ist die bereits erfolgte Umrechnung in physikalische Größen sowie die Portierung des speziellen Protokolls der Steuerserver auf das einheitliche \gls{epics}-Protokoll besonders hilfreich.



\subsection*{Empfehlungen}

Aus den Erkenntnissen dieser Arbeit lassen sich die folgenden Empfehlungen zur Optimierung der Elektronenkühlung für den zukünftigen Betrieb ableiten:

Zur Verbesserung der transversalen Kühlung ist zu empfehlen, nicht nur die Schwerpunkte von Protonen- und Elektronenstrahl durch eine Orbit-Anpassung genau aufeinander abzustimmen;
Zusätzlich sollte auch das \enquote{Galloping-Zentrum} durch geeignete Einstellung der Korrekturmagnete zentriert werden.
Es ist außerdem elementar, die Larmor-Rotation durch Galloping zu minimieren.
Hier sollten insbesondere die Biege-Sektionen stärker berücksichtigt werden, da die Untersuchung der Geschwindigkeitsverteilung im Elektronenstrahl nahelegt, dass dort signifikante Larmor-Oszillationen höherer Ordnung entstehen.
In diesem Zusammenhang ist eine modellbasierte Einstellung des Elektronenkühlers~\cite{Halama.2017} unerlässlich und sollte integraler Bestandteil dessen Kontrollsystems werden.
\\

Die Natur der Elektronenkühlung zeichnet sich durch ihre besondere Effizienz bei geringen Strahldurchmessern aus.
Insofern ist die Verringerung der Emittanz -- beispielsweise durch stochastische Vor-Kühlung -- ein geeignetes Mittel um die Kühlgeschwindigkeit zu steigern.
Die Kombination beider Verfahren bietet wesentliche Vorteile und sollte ein Standardwerkzeug der Strahlkühlung werden.

Es wäre außerdem denkbar den Strahl schon bei Injektions-Energie mit dem \SI{100}{\kilo\eV} Elektronenkühler zu kühlen oder gar eine mehrstufige Beschleunigung durchzuführen.
Durch die von Beginn an kleinere Emittanz könnte die Kühlung bei Ziel-Energie so verstärkt werden.
%Eventuell ist es sogar möglich den Strahl während der Beschleunigungsphase zu kühlen, in dem Elektronen-Energie und Magnete des Kühlers synchron hochgefahren werden.





\section{Ausblick}

Die Strahlzeit beschränkte sich auf den oben genannten Protonen-Impuls.
Es ist jedoch interessant, die Messungen auch in Abhängigkeit der Energien zu wiederholen und zu vergleichen.
Für eine weitergehende Analyse ist auch ein Vergleich der experimentellen Ergebnisse mit Simulationen noch ausstehend.
Die den Kühlprozess modellierenden Programme stehen zur Verfügung~\cite{Katayama.2009} und können dazu herangezogen werden.
\\

Das im Zuge der kombinierten Kühlung aufgetretene Artefakt im Schottky-Spektrum ist aktuell noch nicht verstanden.
Hier können die Erklärungsansätze durch weitere gezielte Analysen überprüft werden (siehe \cref{schottky-artefact}).
\\

Für die Zukunft ist eine Erweiterung der \acrshort{epics}-Schnittstelle des Elektronenkühlers anzustreben.
Dabei können die bislang nur auslesbaren Parameter noch um eine Steuerungsfunktion erweitert werden (siehe \cref{cooler-ioc}).
Auf diese Weise ist eine weitere Automatisierung des Kühlers in komfortabler Art möglich.
Beispielsweise kann das Hochfahren des Elektronenstroms mit verschiedenen Parametern der Elektronen-Kanone und des Magnetsystems synchronisiert werden, so dass dabei häufig auftretende Teil-Verluste des Elektronenstrahls vermieden werden können.
Durch eine Kopplung diesen Prozesses an das \gls{cosy}-Timing-System kann ein vollautomatisierter Betrieb des Kühlers während des regulären Experimentalbetriebs an \gls{cosy} erreicht werden.
Das offene Design von \gls{epics} ermöglicht außerdem die Weiterverarbeitung der Daten in anderen Systemen.
Als Beispiel ist hier die bereits erwähnte Software zur modellbasierten Einstellung des Kühlers zu nennen.

Es bieten sich somit eine Reihe von Möglichkeiten, die Elektronenkühlung auch in Zukunft noch zu verbessern, weitergehend zu analysieren und auszubauen.








