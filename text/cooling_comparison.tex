\section{Vergleichende Analyse}
\label{comparison}

\subsection{Elektronen- und Stochastische Kühlung}
\label{ecool-precool}

\begin{figure}[b!]
	\centering
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{graphics/plots/comparison/plot_22_variant}
		%\includegraphics[width=\textwidth]{graphics/plots/comparison/cooling_performance_fits_31}
		\caption{Elektronenkühlung}
		\label{fig:ec-sc-cooling-ec}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{graphics/plots/comparison/plot_25_variant}
		%\includegraphics[width=\textwidth]{graphics/plots/comparison/cooling_performance_fits_34}
		\caption{Kombinierte Kühlung}
		\label{fig:ec-sc-cooling-both}
	\end{subfigure}
	\caption[Dynamik der Elektronenkühlung ohne und mit Vor-Kühlung]{
		Dynamik der Elektronenkühlung ohne und mit Vor-Kühlung.
		
		Oben: Elektronenstrom von \SI{0.6}{\ampere} bei \SI{907.9}{\kilo\eV} (Hilfslinie markiert Startpunkt).
		Mitte: Spektrogramm der Impuls-Verteilung (farbig codiert) mit je einem Spektrum zu Beginn (rot) und zum Ende (blau).
		Unten: Horizontale und vertikale Emittanz.
		Die himmelblaue Hinterlegung markiert den Bereich aktiver stochastischer Kühlung.
	}
	\label{fig:ec-sc-cooling}
\end{figure}

Die Elektronenkühlung ist transversal erheblich langsamer als die stochastische Kühlung.
Dafür ist die erreichbare Impulsschärfe wesentlich höher.
Es liegt daher nahe die Vorteile beider Verfahren zu kombinieren, um ein bestmögliches Ergebnis zu erzielen.
\\

\cref{fig:ec-sc-cooling} zeigt zwei Messzyklen, in denen ein Strahl mit \SI{3E8}{} Protonen durch Elektronenkühlung alleine (\subref{fig:ec-sc-cooling-ec}) beziehungsweise beide Verfahren (\subref{fig:ec-sc-cooling-both}) gekühlt wird.
Die in (\subref{fig:ec-sc-cooling-both}) dargestellte transversale stochastische \textbf{Vor-Kühlung} bewirkt eine schnellere Abnahme der Emittanz.
Anschließend kann die Elektronenkühlung das Equilibrium sogar nochmals senken, wobei die transversale Kühlung jetzt deutlich schneller ist.
Longitudinal ist die Impulsverteilung mit Vor-Kühlung symmetrischer und auch schärfer; insbesondere tritt der in (\subref{fig:ec-sc-cooling-ec}) sichtbare Doppel-Peak nicht mehr auf.
Außerdem sinkt die longitudinale Kühlzeit von $\tau=\SI{20.2}{\second}$ auf \SI{7.7}{\second}, wodurch das Equilibrium der Impulsbreite doppelt so schnell erreicht werden kann (siehe \cref{app:sc-precooling}).





\subsection{Cluster-Jet Target}

\begin{figure}[b!]
	\centering
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{graphics/plots/comparison/plot_12}
		\caption{Energieverlust durch Target}
		\label{fig:target-loss}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{graphics/plots/comparison/plot_14}
		\caption{Kompensation mittels Barrier Bucket}
	\end{subfigure}
	\caption[Einfluss von Target und Barrier Bucket auf den Strahl]{
		Einfluss von Target und Barrier Bucket auf den Strahl.
		
		Oben: Rate der Proton-Target Wechselwirkung und Angabe der Target"=Dichte.
		Mitte: Impuls-Spektrogramm mit Spektren an Marker"=Positionen.
		Unten: Zahl gespeicherter Protonen (grün) und Emittanz.
	}
	\label{fig:target-bb}
\end{figure}

Das Cluster-Jet Target~\cite{cjt}, welches beim PANDA-Experiment~\cite{panda} am \gls{hesr} zum Einsatz kommen wird, ist derzeit für Testzwecke in \gls{cosy} installiert.
Wasserstoff wird bei \SI{18}{\bar} und etwa \SI{25}{\kelvin} durch eine Lavaldüse gepresst, in der sich nanometergroße Cluster bilden~\cite{Khoukaz.2012}.
Der Cluster-Strahl strömt von oben in die \SI{2.1}{\meter} weiter unten liegende Versuchskammer, wo der Protonen-Strahl ihn senkrecht auf einer Länge von \SI{17}{\milli\meter} durchtritt\footnotemark{}.
Die Atom-Flächendichte $\rho_T$ des so erzeugten Targets liegt bei \SIrange[per-mode=reciprocal]{e13}{2e15}{\per\square\centi\meter}.
\footnotetext{B. Hetz, WWU Münster, Persönliche Kommunikation, Oktober 2018}%

Durch Streuung an den Wasserstoff-Clustern verliert der umlaufende Teilchenstrahl Energie und gleichzeitig wächst die Emittanz stark an.
In \cref{fig:target-loss} ist dies gut zu erkennen.
Die im oberen Diagramm jeweils aufgetragene Rate der Proton-Target Interaktion (braun) besitzt dabei qualitative Gültigkeit und gibt den Einschaltzeitpunkt des Jets an.
Der dadurch auftretende Strahlverlust zeigt sich in der abnehmenden Anzahl im Beschleuniger gespeicherter Protonen (grün, unteres Diagramm).
Der Energieverlust geht schließlich über die Akzeptanz des Beschleunigers hinaus, bis der Strahl bei $\gamma<\gamma_{tr}$ vollständig verloren geht (hier nicht sichtbar, siehe \cref{app:fig:target-gamma-tr-crossing} im Anhang).
Eine Kompensation dieser Effekte ist daher essentiell.



\subsubsection*{Barrier Bucket}

Das \glsfmt{\emph}{\gls{bb}}~\cite{A.V.Smirnov.} bezeichnet eine gepulste Hochspannung, die in Frequenz und Phase so auf den umlaufenden Strahl abgestimmt ist, dass die Teilchen zwischen den zwei zeitlich getrennten Pulsen (\emph{Barrier}) wie in einem Behälter (\emph{Bucket}) eingeschlossen sind.
Teilchen mit verringerter Energie sind langsamer und \enquote{fallen zurück}, bis sie vom Spannungspuls am Ende des \emph{Bucket} wieder beschleunigt werden.
Umgekehrt werden auch Teilchen mit zu hoher Energie durch den (negativen) Anfangs-Puls gebremst.
Im vorliegenden Experiment wurde das \emph{Bucket} fast auf den gesamten Umfang ausgedehnt, so dass der End-Puls unmittelbar vor dem (nächsten) Anfangs-Puls liegt.
Somit ist das \gls{bb} in der Lage den Impuls sowie die Frequenz des umlaufenden Strahls zu stabilisieren.
\\

Das \acrlong{bb} kann zwar den mittleren Energieverlust durch das Target kompensieren; es ist jedoch nicht in der Lage der Phasenraumvergrößerung entgegenzuwirken.
Daher bleibt zwar der Impuls des umlaufenden Strahls konstant, die Emittanzen wachsen jedoch fast im selben Maße wie ohne \gls{bb} und auch die Impulsverteilung wird breiter (\cref{fig:target-bb}).



\subsubsection*{Strahlkühlung und Barrier Bucket mit Target}

\begin{figure}
	\centering
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{graphics/plots/comparison/plot_0}
		\caption{Keine Kühlung}
		\label{fig:target-bb-cool-none}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{graphics/plots/comparison/plot_1}
		\caption{Elektronenkühlung}
		\label{fig:target-bb-cool-ec}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{0.48\textwidth}
		\includegraphics[width=\textwidth]{graphics/plots/comparison/plot_2}
		\caption{Kombinierte Kühlung}
		\label{fig:target-bb-cool-ec-sc}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\centering
		\def\arraystretch{1.1}
		\sisetup{table-number-alignment=center,table-figures-integer=2,table-figures-decimal=1,table-figures-exponent=2}%
		\begin{tabular}{lS>{$\to$}cS}
			\multicolumn{4}{c}{(\subref{fig:target-bb-cool-none})} \\ \hline
			Sigma: & 6.9e-4 &  & 11.1e-4 \\
			RMS:   & 6.5e-4 &  & 12.8e-4
		\end{tabular}
		\par\bigskip
		\begin{tabular}{lS>{$\to$}cS}
			\multicolumn{4}{c}{(\subref{fig:target-bb-cool-ec})} \\ \hline
			Sigma: & 7.1e-4 &  & 5.3e-4 \\
			RMS:   & 6.6e-4 &  & 8.5e-4
		\end{tabular}
		\par\bigskip
		\begin{tabular}{lS>{$\to$}cS}
			\multicolumn{4}{c}{(\subref{fig:target-bb-cool-ec-sc})} \\ \hline
			Sigma: & 8.3e-4 &  & 2.1e-4 \\
			RMS:   & 8.6e-4 &  & 6.5e-4
		\end{tabular}
		\par\bigskip
		\caption{Relative Impulsbreiten $\delta$}
		\label{table:target-bb-cool}
	\end{subfigure}
	\caption[Strahlkühlung mit Barrier Bucket und Cluster-Jet Target]{
		Strahlkühlung mit Barrier Bucket und Cluster-Jet Target.

		Dargestellt jeweils:
		Oben: Elektronenstrom (schwarz) und Target"=Rate (braun).
		Mitte: Impuls-Spektrogramm.
		Unten: Teilchenzahl im Ring (grün) und Emittanz.
		Tabelle \subref{table:target-bb-cool} gibt die relativen Impulsbreiten zu Beginn und zum Ende der Messungen an.
	}
	\label{fig:target-bb-cool}
\end{figure}

Um der Impulsverbreiterung entgegenzuwirken wird die Elektronenkühlung eingesetzt.
Bei einer Target-Dichte von \SI[per-mode=reciprocal]{1.1+-0.02 e15}{\per\square\centi\meter} durchgeführte Messreihen mit \gls{bb} sind in \cref{fig:target-bb-cool} gezeigt.
Die Elektronenkühlung ist in der Lage ein Aufweiten des Strahls in longitudinaler Richtung zu verhindern und den Strahl stattdessen entgegen dem Einfluss des Targets zu kühlen (\cref{fig:target-bb-cool-none,fig:target-bb-cool-ec}).
Das transversale Emittanzwachstum kann jedoch durch den Elektronenkühler nicht kompensiert werden, was schließlich auch zum Nachlassen der longitudinalen Kühlung führt.
Auch der Protonenstrahl erleidet weiterhin starke Verluste, was an der stark abnehmenden Zahl im Ring gespeicherter Teilchen zu sehen ist.
\\

Erst durch die Kombination mit transversaler stochastischer Kühlung kann auch hier die Emittanz verringert und konstant gehalten werden (\cref{fig:target-bb-cool-ec-sc}).
Obwohl diese keinen direkten Einfluss auf die Impulsverteilung hat, zeigt sich eine wesentlich stärkere longitudinale Kühlung.
So ist die Impulsbreite (Sigma-Wert des Gauß-Fits) nach \SI{500}{\second} Elektronenkühlung statt um \SI{25}{\percent} nun um \SI{75}{\percent} verringert.

Daraus kann geschlossen werden, dass die Effizienz der longitudinalen Elektronenkühlung stark von der transversalen Emittanz beeinflusst wird.
Es ist daher umso mehr relevant, die transversale Kühlung zu verbessern.
Insgesamt ist auch der Strahlverlust bei Kombination beider Kühl-Verfahren wesentlich reduziert, wodurch eine möglichst ergiebige Nutzung des Protonenstrahls für den Experimentalbetrieb erreicht wird.
\\

Weitere Messreihen bei einer Target-Dichte von \SI[per-mode=reciprocal]{4E14}{\per\square\centi\meter} zum Vergleich der Auswirkung von \acrlong{bb} und Kühlung liegen im Anhang bei (\cref{app:fig:target-bb-cool}).



%\FloatBarrier
\subsubsection*{Strahlkühlung bei reduzierter Target-Dichte}

\begin{figure}
	\centering
	\begin{subfigure}{0.48\textwidth}
		% Timestamp: 2018-10-26 16:08:07.047165+00:00
		\includegraphics[width=\textwidth]{graphics/plots/comparison/plot_7}		\caption{Keine Kühlung}
		\label{fig:target-cool-none}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		% Timestamp: 2018-10-26 16:18:51.051428+00:00
		\includegraphics[width=\textwidth]{graphics/plots/comparison/plot_9}
		\caption{Elektronenkühlung}
		\label{fig:target-cool-ec}
	\end{subfigure}
	\par\bigskip
	\begin{subfigure}{0.48\textwidth}
		% Timestamp: 2018-10-26 16:40:19.060237+00:00
		\includegraphics[width=\textwidth]{graphics/plots/comparison/plot_10}
		\caption{Kombinierte Kühlung}
		\label{fig:target-cool-ec-sc}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\centering
		\def\arraystretch{1.1}
		\sisetup{table-number-alignment=center,table-figures-integer=1,table-figures-decimal=2,table-figures-exponent=2}%
		\begin{tabular}{lS>{$\to$}cS}
			\multicolumn{4}{c}{(\subref{fig:target-cool-none})} \\ \hline
			Sigma: & 6.2e-4 &  & 5.8e-4 \\
			RMS:   & 8.1e-4 &  & 6.3e-4
		\end{tabular}
		\par\bigskip
		\begin{tabular}{lS>{$\to$}cS}
			\multicolumn{4}{c}{(\subref{fig:target-cool-ec})} \\ \hline
			Sigma: & 5.8e-4 &  & 0.47e-4 \\
			RMS:   & 5.4e-4 &  & 9.7e-4
		\end{tabular}
		\par\bigskip
		\begin{tabular}{lS>{$\to$}cS}
			\multicolumn{4}{c}{(\subref{fig:target-cool-ec-sc})} \\ \hline
			Sigma: & 6.1e-4 &  & 0.29e-4 \\
			RMS:   & 6.2e-4 &  & 4.3e-4
		\end{tabular}
		\par\bigskip
		\caption{Relative Impulsbreiten $\delta$}
		\label{table:target-cool}
	\end{subfigure}
	\caption[Strahlkühlung bei reduzierter Target-Dichte]{
		Strahlkühlung bei reduzierter Target-Dichte.
		
		Dargestellt jeweils:
		Oben: Elektronenstrom (schwarz) und Target"=Rate (braun).
		Mitte: Impuls-Spektrogramm.
		Unten: Emittanz und Teilchenzahl im Ring (grün).
		Tabelle (\subref{table:target-cool}) gibt die relativen Impulsbreiten zu Beginn und zum Ende der Messungen an.
	}
	\label{fig:target-cool}
\end{figure}

Bei einer reduzierten Target-Dichte von \SI[per-mode=reciprocal]{2E14}{\per\square\centi\meter} ist der Einsatz des \acrlong{bb} nicht notwendig.
Die zwischen kalten Elektronen und Protonen durch Coulomb"=Wechselwirkung übertragene Energie reicht hier aus, um den mittleren Energieverlust zu kompensieren (siehe \cref{fig:target-cool-none,fig:target-cool-ec}).
Ebenso kann das Emittanzwachstum kompensiert und ein Gleichgewichtszustand hergestellt werden.
Es existiert jedoch eine Klasse von Protonen mit verringertem Impuls, die bei Aktivierung des Targets nicht von der Elektronenkühlung erfasst wird, so dass sich zwischenzeitlich ein Doppel-Peak in der Impulsverteilung ergibt.
Während bei Nenn-Impuls eine starke Kühlung bis $\Delta p / p = \SI{4.7E-5}{}$ erfolgt, verlieren die nicht gekühlten Protonen durch Target-Wechselwirkung kontinuierlich Energie und gehen schließlich verloren.
\\

Auch in dieser Konfiguration wird erneut eine Messung mit transversaler stochastischer Vor-Kühlung (\cref{fig:target-cool-ec-sc}) durchgeführt.
Neben der durch die stochastische Kühlung hervorgerufenen signifikanten Minimierung der Emittanz ist hier vor allem das verbesserte longitudinale Verhalten bemerkenswert.
Es zeigt sich eine äußerst starke Impulsschärfung durch die Elektronenkühlung.
Die relative Impulsbreite sinkt um \SI{95}{\percent} von \SI{6.1E-4}{} (rotes Spektrum bei $t=\SI{10}{\second}$) auf \SI{2.9E-5}{} (blaues Spektrum bei $t=\SI{560}{\second}$).
Zusätzlich ist der ohne stochastische Kühlung sichtbare Verlust einiger ungekühlter Protonen nicht mehr zu beobachten.

Da die stochastische Kühlung keine longitudinalen Effekte hat, ist auch hier die beobachtete Verbesserung in der Elektronenkühlung allein auf die geringere Emittanz zurückzuführen.
Die longitudinale Kühlung scheint außerdem auch schneller stattzufinden, was aber aufgrund des im nächsten Absatz diskutierten Artefakts im Schottky-Spektrum nicht eindeutig festgestellt werden kann.
Nach dem Auftreten der Störung ist das Equilibrium jedoch bereits erreicht, weshalb der transiente Kühlprozess vollständig innerhalb des nicht auswertbaren Bereiches stattgefunden haben muss.
Am Beispiel einer Messung ohne Target wurde bereits in \cref{ecool-precool} gezeigt, dass eine geringere Emittanz prinzipiell eine wesentlich schnellere longitudinale Elektronenkühlung bewirkt.



\subsection{Schottky Artefakt}
\label{schottky-artefact}

Bei den Messungen mit kombinierter stochastischer und Elektronenkühlung wurde eine Störung im Schottky-Spektrum beobachtet, welche unter anderem in \cref{fig:target-cool-ec-sc} auftritt.
Auffällig ist, dass das Rauschen hauptsächlich für kleinere Energien ($p<p_0$) auftritt, das heißt für Frequenzen oberhalb der nominalen Umlauffrequenz.
Während das Artefakt bei Messungen ohne \acrlong{bb} besonders ausgeprägt ist, kann auch mit \gls{bb} ein verstärktes Rauschen innerhalb der Impulsverteilung des Strahls beobachtet werden (siehe \cref{fig:target-bb-cool-ec-sc}).
\\

Da derartige Energie-Sprünge einzelner Protonen äußerst unwahrscheinlich sind, lässt sich vermuten, dass es sich dabei nur um einen Effekt der Messung handelt.
Dafür spricht auch, dass weder in der Teilchenzahl noch im \gls{ipm}-Signal korrelierende Effekte beobachtbar sind.
Die zeitgleiche Störung in der horizontalen Emittanz ist rein rechnerisch zu begründen, da die Impulsbreite aufgrund der Dispersion in die Formel zur Berechnung der Emittanz einfließt (siehe \cref{eq:beamsize}).
Die vertikale Dispersion ist hingegen $D^p_x\approx 0$, so dass der Einfluss hier nichtig ist.

Aufgrund der Tatsache, dass das Schottky-Spektrum am stochastischen \emph{Pick-Up} gemessen wurde, der zusammen mit dem \emph{Kicker} sowie dem Strahl den geschlossenen Regelkreis der stochastischen Kühlung bildet, könnte das Artefakt auf eine Rückkopplung zurückzuführen sein.
Möglicherweise spielt auch eine nicht exakte Abstimmung der kinetischen Energie der Elektronen und der Frequenz der stochastischen Kühlung eine Rolle, da beide den Protonen-Impuls beeinflussen.

Der Effekt kann jedoch zum jetzigen Zeitpunkt nicht erklärt werden und bedarf genauerer Erforschung.
In diesem Zusammenhang ist sowohl eine Vergleichsmessung mit einem unabhängigen \emph{Pick-Up} interessant, sowie auch eine systematische Untersuchung etwaiger Abhängigkeiten von Teilchenzahl, kinetischer Elektronenenergie und Frequenz der stochastischen Kühlung.



%\FloatBarrier
\subsection{Zusammenfassung}

In den hier aufgeführten Messungen konnte gezeigt werden, dass sich der Energieverlust durch das Target bei einer Dichte von \SI[per-mode=reciprocal]{2E14}{\per\square\centi\meter} durch die Kühlung allein; bei höheren Dichten durch das \acrlong{bb} kompensieren lässt.
Mithilfe der Elektronenkühlung kann der longitudinale Phasenraum stark verkleinert werden, was zu einer starken Schärfung der Impulsbreite führt.
In transversaler Ebene besteht jedoch Optimierungspotential. So kann die Emittanz bei reduzierter Target-Dichte zwar gehalten, das Emittanzwachstum bei \SI[per-mode=reciprocal]{1E15}{\per\square\centi\meter} jedoch nicht kompensiert werden.
\\

Das beste Ergebnis wurde durch die Kombination der Vorteile von Elektronenkühlung und (transversaler) stochastischer Kühlung beobachtet.
Im Allgemeinen ist die stochastische Kühlung bei großer Emittanz besonders effizient, die Elektronenkühlung arbeitet hingegen bei geringer Emittanz besonders gut~\cite{Mohl.1985}.
Somit kann der Protonenstrahl stochastisch vor-gekühlt werden, um den Phasenraum anschließend durch Elektronenkühlung noch stärker zu komprimieren.

Bei den Messungen konnte die Emittanz durch die stochastische Kühlung auf unter \SI{0.5}{\milli\meter\milli\radian} reduziert und gehalten werden;
bei reduzierter Target-Dichte sogar unter \SI{0.2}{\milli\meter\milli\radian}.
Durch die geringe Emittanz ist dann sowohl die longitudinale als auch die transversale Elektronenkühlung deutlich schneller und es können kleinere Equilibiumswerte erreicht werden (siehe auch \cref{app:sc-precooling}).
So wurden mit der Elektronenkühlung relative Impulsbreiten von \SI{2.9E-5}{} ($1\sigma$-Intervall bei reduzierter Dichte) beziehungsweise \SI{2.1E-4}{} (bei einer Target-Dichte von \SI[per-mode=reciprocal]{1E15}{\per\square\centi\meter}) erreicht, was mit der stochastischen Kühlung alleine nicht möglich ist.

Im Zusammenspiel beider Kühlsysteme konnten die Strahlverluste -- auch für hohe Target-Dichten -- von etwa \SI{40}{\percent} innerhalb von \SI{500}{\second} ohne Kühlung auf etwa \SI{10}{\percent} reduziert werden.
Dies erhöht nicht nur die für spätere Experimente zur Verfügung stehende Luminosität, sondern steigert auch die Speicherdauer und somit die integrierte Luminosität wesentlich.
Zusätzlich steigt die Signifikanz der Messungen durch die geringe Impulsunschärfe.
\\

Die Strahlkühlung im Target-Betrieb ist insbesondere für das PANDA-Experiment am \gls{hesr} von Bedeutung, bei dem Impulsbreiten von \SI{E-5}{} benötigt werden und eine möglichst lange Nutzung des Antiprotonen-Strahls angestrebt wird~\cite{panda.beam}.

Eine Kombination aus stochastischer und Elektronenkühlung -- wie sie hier gezeigt wurde -- ist daher zu empfehlen und in der Lage, die gestellten Anforderungen zu erfüllen.
Dabei bedarf das oben diskutierte Schottky-Artefakt weiterer Forschung.
Gleichzeitig ist auch die Optimierung der transversalen Elektronenkühlung sowie ein besseres Verständnis selbiger wichtig, womit sich der nächste Abschnitt eingehend beschäftigt.
%\\
%
%Abschließend sei noch erwähnt, dass es denkbar wäre die Elektronenkühlung in zwei Phasen vorzunehmen.
%Dabei könnte der Protonenstrahl in zwei Stufen beschleunigt und jeweils gekühlt werden, da die Elektronenkühlung stark Energie-Abhängig ist~\cite{Mohl.2005}.
%Der Vorteil läge darin, dass die Kühlung bei der niedrigeren Energie bereits eine transversale Phasenraumkompression erzeugen kann, welche die anschließende Kühlung bei hoher Energie verbessert.
%Ein schneller Wechsel zwischen zwei Energien ist mit dem \zmev Elektronenkühler jedoch derzeit noch nicht möglich, wäre aber auf Basis der \gls{epics}-Integration in das \gls{cosy}-Kontrollsystem denkbar, wenn die Implementierung um die noch fehlenden Steuer-Parameter ergänzt wird.



