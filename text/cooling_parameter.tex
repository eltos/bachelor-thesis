\section{Bestimmung der charakteristischen Kühlparameter}
\label{cooling-parameter}


\subsection{Longitudinale Elektronenkühlung}
\label{cooling_long}

Zur Charakterisierung der longitudinalen Kühlung dient die in \cref{schottky} beschriebene Strahldiagnose mittels Schottky-Pick-Up.
Die Datenerfassung und Auswertung wurde bereits in \cref{schottky-ioc} erläutert und stellt die Grundlage der hier dargestellten Messungen dar.
\\

\cref{fig:cooling_eq_long} zeigt die longitudinale Elektronenkühlung eines Strahls mit \SI{1.6E9}{} Protonen bei $p=\SI{2425}{\mega\eV\per\c}$.
Dargestellt ist der zeitlichen Verlauf im Schottky-Spektrum sowie die daraus abgeleitete Impulsbreite.
Bereits kurz nach Hochfahren des Elektronenstroms zeigt sich die Kühlung deutlich in der exponentiellen Abnahme der Impulsbreite~$\Delta p / p$ bis zum Gleichgewicht von Kühlung und Aufheizeffekten.
Auch in Spektrum selbst (rechts) ist die \enquote{Schärfung} der Frequenzverteilung von $\Delta f / f = \SI{2E-5}{}$ (rot) auf $\SI{1.8E-6}{}$ (blau) zu erkennen, was einem zehnfach schärfer bestimmten Protonen-Impuls entspricht.
\\

\begin{figure}[b!]
	\centering
	\includegraphics[width=\textwidth]{graphics/plots/cooling/cooling_performance_profiles_7_long}
	\caption[Schottky-Spektrum während der Elektronenkühlung]{
		Schottky-Spektrum während der Elektronenkühlung.
		Dargestellt ist der Elektronenstrom (oben), das Frequenz-Spektrogramm (mitte, Amplitude farblich codiert) und die longitudinale Impulsbreite (unten);
		außerdem zwei Spektren (rechts), die zu Beginn (rot) und zum Ende (blau) der Kühlung aufgenommen wurden.
		Es wurden $\SI{1.6E9}{}$ Protonen bei $p=\SI{2425}{\mega\eV\per\c}$ durch einen Elektronenstrahl von $I=\SI{612}{\milli\ampere}$ und $E_{kin}=\SI{907.9}{\kilo\eV}$ gekühlt.
	}
	\label{fig:cooling_eq_long}
\end{figure}

Auffällig ist, dass sich die im Gleichgewicht zu erwartende Konstanz der Impulsbreite nur bei den aus dem Gauß-Fit ermittelten Werten zeigt, während der RMS-Wert hier einen linearen Anstieg aufweist.
Dies kann durch eine Klasse von Protonen mit geringerer Energie erklärt werden, die nicht von der Kühlung erfasst wurden.
Sie erleiden daher einen konstanten Energieverlust, der unter anderem auf Kollisionen mit dem Restgas zurückgeführt werden kann.
Der Effekt ist im mittleren Diagramm als weg-driftende pink-farbene Spur und schließlich auch im Spektrum (rechts, blau) zu erkennen.

In der Übergangszeit bei etwa \SIrange{100}{200}{s} ist das Spektrum dadurch nicht rein gaußförmig, sondern weißt einen Doppel-Peak auf, wodurch die ermittelte Impulsbreite fluktuiert.
Danach bildet der Gauß-Fit jedoch den Strahlkern nach, dessen Impulsbreite im Gleichgewicht wie zu erwarten konstant bleibt.
Die abdriftende Teilchen-Klasse verschwindet dann im Untergrund (siehe blaues Spektrum rechts).

Um diesem Effekt Rechnung zu tragen, wurde der exponentiellen Näherung (gestrichelte Linie) für den RMS-Wert ein linearer Term hinzugefügt:
\begin{equation}
	\label{eq:dpp_fit}
	\delta(t)
	= \delta_{eq}
	+ \left( \delta_0 - \delta_{eq} \right) e^{-t/\tau}
	+ R t
\end{equation}
Hierbei wird die relative Impulsbreite~$\delta=\Delta p / p$ im Equilibrium ($t\to\infty$) durch $\delta_{eq}$ und bei $t=0s$ durch $\delta_0$ beschrieben.
$\tau$ ist die Zeitkonstante der longitudinalen Kühlung und $Rt$ der oben erläuterte lineare Term.
Das Equilibrium wird bei etwa $5\tau$ erreicht.
\cref{table:cooling_eq_long} listet die für obige Messung auf Basis von \cref{eq:dpp_fit} gefundenen Parameter auf.
\\

Die longitudinale Kühlung ist mit der Verringerung der Impulsbreite um mehr als eine Größenordnung auf \SI{5.4E-5}{} innerhalb von $5\tau\approx\SI{100}{s}$ nicht nur vergleichsweise schnell, sondern setzt auch unmittelbar mit Hochfahren des Elektronenstroms ein.
Wie wir im nächsten Abschnitt sehen werden, trifft dies auf die transversale Kühlung nicht zu.


\begin{table}
	\centering
	\def\arraystretch{1.1}
	% Ergebnisse Messung #7 (Zeitstempel 2018-10-20 14:29:05 +00:00)
	\begin{tabular}{cSSs}
		Parameter     & {Gauß-Fit}    & {RMS-Wert}    & {Einheit} \\\hline
		$\delta_0$    & 67.3+-0.1 E-5 & 54.6+-0.3 E-5 &           \\
		$\delta_{eq}$ &  5.4+-0.1 E-5 & 17.7+-0.1 E-5 &           \\
		$\tau$        & 18.6+-0.6     & 31.7+-0.5     & s         \\
		$R$           &   {---}       &  7.6+-0.2 E-8 & s^{-1}
	\end{tabular}
	\caption{Ergebnisse der longitudinalen Elektronenkühlung}
	\label{table:cooling_eq_long}
\end{table}





\clearpage
\subsection{Transversale Elektronenkühlung}
\label{cooling_trans}

Zur Charakterisierung der transversalen Kühlung dient die in \cref{ipm_profile} beschriebene Strahldiagnose mittels \gls{ipm}.
Die Ableitung der Emittanz aus dem Profil erfolge nach \cref{eq:beamsize} unter Berücksichtigung der relative Impulsbreite.
\\

\cref{fig:cooling_eq_trans} zeigt die transversale Elektronenkühlung zu der im vorhergehenden Abschnitt beschriebenen Messung.
Dargestellt ist das horizontale und vertikale Strahlprofil sowie die daraus jeweils ermittelte Emittanz.
Durch die Kühlung verringert sich der Strahldurchmesser am \gls{ipm} horizontal von $\sigma_x=\SI{2.5}{mm}$ auf $\SI{1.4}{mm}$ und vertikal von $\sigma_y=\SI{2.2}{mm}$ auf $\SI{0.9}{mm}$.
Die transversale Phasenraum-Kompression zeigt sich im Verlauf der Emittanz.
Diese nimmt zunächst linear ab, bevor sie sich asymptotisch dem Eqilibriums-Wert nähert.
\\

\begin{figure}[b!]
	\centering
	\includegraphics[width=0.9\textwidth]{graphics/plots/cooling/cooling_performance_profiles_7_trans}
	\caption[Strahlprofil während der Elektronenkühlung]{
		Strahlprofil während der Elektronenkühlung.
		Dargestellt ist der Elektronenstrom (oben), das horizontale und vertikale Strahlprofil über Zeit (mitte, Amplitude farblich codiert) und die Emittanz (unten);
		außerdem je zwei transversale Profile (rechts), die vor (rot) und zum Ende (blau) der Kühlung aufgenommen wurden.
		Die Strahlparameter entsprechen \cref{fig:cooling_eq_long}
	}
	\label{fig:cooling_eq_trans}
\end{figure}

Der transiente Prozess lässt sich durch eine nichtlineare, zweidimensionale \gls{dgl} erster Ordnung beschreiben:
\begin{equation}
\label{eq:emittance_fit_dgl}
	\frac{\partial}{\partial t} \epsilon_{x,y}
	= - \lambda \epsilon_{x,y}
	  + \frac{D_{x,y}}{\sqrt{\epsilon_x \epsilon_y}}
\end{equation}
\begin{equation}
	\lambda
	= \frac{\epsilon_\lambda}{T_{x,y} \left(\epsilon_\lambda+\epsilon_x+\epsilon_y\right)^{3/2}}
\end{equation}
\begin{quote}
	\enquote{%
		The first term on the right describes cooling (emittance decay with damping ratio~$\lambda$);
		the second, intra beam scattering and other \enquote{heating} factors, which in this model are represented as diffusion with constant coefficients~$D_{x,y}$.
		Denominator~$\sqrt{\epsilon_x\epsilon_y}$ stands for coupling between the diffusion rate and ion beam density $n$ (that is, $n\sim1/\sqrt{\epsilon_x\epsilon_y}$).}
	\cite[S.~1229]{kamerdzhiev.2015}%
	\footnote{Übersetzung des Autors:
		Der erste Term rechts beschreibt den Kühlvorgang (Abklingen der Emittanz mit der Dämpfungskonstante~$\lambda$);
		der zweite \emph{Intra Beam Scattering} und andere Heiz-Faktoren, die in diesem Modell als Diffusion mit konstanten Koeffizienten~$D_{x,y}$ vertreten sind.
		Der Nenner~$\sqrt{\epsilon_x\epsilon_y}$ steht für die Kopplung zwischen Diffusionsrate und Dichte $n$ des Ionenstrahls.}
\end{quote}

Da sich das Gleichungssystem nicht auf einfache Weise analytisch lösen lässt, wird eine numerisch-iterative Anpassung (gestrichelte Linien) durchgeführt.
Diese liefert neben den unbekannten Parametern der \gls{dgl} auch die Startwerte bei $t=0$.
Die Emittanz im Equilibrium lässt sich dann für $t\to\infty$ errechnen.
\cref{table:cooling_eq_trans} listet die so ermittelten Parameter für obige Messung auf.
In \cref{app:emittance-fit} ist ein Vergleich verschiedener Modelle zur Beschreibung der Emittanzabnahme angefügt.

Für die transversale Strahlkühlung benötigt der Elektronenkühler mit $10 T\approx\SI{10}{\minute}$ eine im Vergleich zur longitudinalen Kühlung lange Zeitspanne.
\\

In der in \cref{app:ec-eq} angefügten Messung wurde die Messdauer auf etwa 20 Minuten verlängert, so dass der Gleichgewichtszustand transversal deutlich besser bewertet werden kann.
Auch hier zeigt sich in der Gegenüberstellung der longitudinalen und transversalen Kühlung (\cref{app:ec-eq-figure}) deutlich die unterschiedliche Zeit-Skala beider Prozesse.
Dieses Phänomen wird in den folgenden Abschnitten eingehend untersucht und näher beleuchtet.


\begin{table}
	\centering
	\def\arraystretch{1.1}
	% Ergebnisse Messung #7 (Zeitstempel 2018-10-20 14:29:05 +00:00)
	\begin{tabular}{cSSs}
		Parameter          & {Horizontal}  & {Vertikal}     & {Einheit}     \\\hline
		$\epsilon_0$       & 0.255+-0.002  & 0.705+-0.002   & mm.mrad       \\
		$\epsilon_{eq}$    & 0.074+-0.007  & 0.108+-0.009   & mm.mrad       \\
		$T$                & 50+-4         & 71+-5          & s             \\
		$D$                & 7+-4          & 7+-3           & (mm.mrad)^2/s \\
		$\epsilon_\lambda$ & \multicolumn{2}{S}{0.32+-0.02} & mm.mrad
	\end{tabular}
	\caption{Ergebnisse der transversalen Elektronenkühlung}
	\label{table:cooling_eq_trans}
\end{table}


