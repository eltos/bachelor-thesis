\section{Geschwindigkeits-Profil des Elektronenstrahls}
\label{cooling-profile}


\subsection{Methode}
Bei der Optimierung der Einstellungen des \zmev Elektronenkühlers konnte eine Abhängigkeit der Protonen-Umlauffrequenz im Schottky-Spektrum von der Lage des Elektronen-Orbits in der Kühlsektion beobachtet werden.
Dieses Phänomen soll nun genauer studiert werden.

Dazu wird der Protonen-Strahl zunächst bis zum Gleichgewichtszustand gekühlt, so dass dessen Strahlgröße mit $\sigma<\SI{1}{mm}$ im Vergleich zum Elektronenstrahl klein ist (\emph{Pencil-Beam}).
Anschließend wird der Elektronen-Orbit gezielt manipuliert und so die relative Lage des Protonenstrahls im Elektronenstrahl verändert.
Aus der Messung der Umlauffrequenz lassen sich dann Rückschlüsse auf den Teil des Elektronenstrahls ziehen, der jeweils mit den Protonen wechselwirkt.
Ermittelt man aus der Frequenz den Impuls und daraus die (longitudinale) Geschwindigkeit der Protonen, lässt sich diese als Funktion der Elektronenstrahl-Position darstellen.
Da die longitudinalen Komponenten der Protonen- und Elektronengeschwindigkeit gleich groß sind (siehe \cref{ecool}), ergibt sich ein \enquote{Geschwindigkeits-Profil}.






\subsection{Bestimmung der Orbit-Response-Matrix}
\label{scan-orm}
Die transversale Verschiebung des Elektronenstrahls ist mithilfe der im Vorfeld dieser Arbeit entwickelten Software (siehe \cref{cooler-ioc}) vergleichsweise einfach und schnell zu realisieren.
Dabei wird die Stärke des Magnetfeldes der beiden Korrekturspulen \enquote{LINE17HOR-1} und \enquote{LINE17VER-1} in der geraden Sektion (siehe \cref{fig:cooler}) gezielt verändert.
Die resultierende Ablenkung der Elektronen über eine Länge von \SI{1.7}{m} erfolgt dabei homogen auf den Strahl als Ganzes.
Da das $x$-$y$"=Koordinatensystem der Ablenkmagnete gegenüber der Kühlsektion um \ang{45} gedreht ist, müssen für eine dortige horizontale oder vertikale Verschiebung jeweils beide Spulen angesteuert werden.
\\

Die \glsfmt{\emph}{\gls{orm}} beschreibt die Änderung des Elektronen-Orbits in der Kühlsektion (zwischen BPM~6 und 7) infolge einer Änderung der Stromstärke~$I$ an den Korrekturspulen:
\begin{equation}
\label{eq:orm}
	\begin{pmatrix} \Delta x \\ \Delta y \end{pmatrix} =
	\matr{A}
	\begin{pmatrix} \Delta I_{HOR} \\ \Delta I_{VER} \end{pmatrix}
\end{equation}
Dabei ist $\matr{A} \in\mathbb{R}^{2 \times 2}$ die \gls{orm}, $I_{HOR}$ und $I_{VER}$ sind die Stromstärken der beiden Korrekturspulen und $(x,y)$ ist die Lage des Elektronenstrahls (an BPM~6).
Wird nun $I_{HOR}$ variiert (bei $I_{VER}=\text{const}$), so gilt:
\begin{align}
\label{eq:orm_linear_fit}
	\Delta x = a_{1,1} \Delta I_{HOR}
	&& \text{und} &&
	\Delta y = a_{2,1} \Delta I_{HOR}
\end{align}
Durch Messung von $(x,y)$ für verschiedene Stromstärken und lineare Regression kann somit die erste Spalte der Matrix bestimmt werden.
Die zweite folgt analog durch Variation von $I_{VER}$.
Beide Messreihen sind in \cref{app:orm} beigefügt.
\\

Nach Inversion der gemessenen \acrlong{orm} lässt sich dann umgekehrt die nötige Stromstärkenänderung für eine gewünschte Orbit-Verschiebung ermitteln:
\begin{equation}
\label{eq:orm_inv}
	\begin{pmatrix} \Delta I_{HOR} \\ \Delta I_{VER} \end{pmatrix} =
	\begin{pmatrix}\begin{tabular}{@{}*{3}{S[table-format=+1.4]}@{}}
		 0.3176 & 0.5395 \\
		-0.1683 & 0.3364
	\end{tabular}\end{pmatrix}
	\si[per-mode=fraction]{\ampere\per\milli\meter}
	\begin{pmatrix} \Delta x \\ \Delta y \end{pmatrix}
\end{equation}
Diese Matrix weißt die typische Vorzeichen-Struktur einer Rotations-Matrix auf, was auf die Eingangs erwähnte Drehung des Koordinatensystems zwischen Korrekturspulen und Kühlstrecke zurückzuführen ist.

Mit einem einfachen Skript, welches die obige Gleichung implementiert (siehe \cref{app:orbit_shift}), kann der Elektronen-Orbit in der Kühlstrecke somit beliebig in horizontaler und vertikaler Richtung verschoben werden.





\subsection{Messreihen und Auswertung}

Für die Messung wird der Orbit -- nach der vollständigen Kühlung des Strahls -- in Schritten von \SI{300}{\micro\meter} in positiver sowie negativer, horizontaler beziehungsweise vertikaler Richtung verschoben.
Die Strahlposition an BPM~6 sowie das Schottky-Spektrum wird dabei jeweils über etwa \SI{30}{\second} aufgenommen, um ausreichend Statistik zu sammeln.
Eine beispielhafte Messung zeigt \cref{fig:orbit_scan_data_raw} im Anhang.
In der sprunghaften Änderung der Protonen-Umlauffrequenz zeigt sich die Energieübertragung zwischen Elektronen und Protonen.
Dabei werden letztere gebremst beziehungsweise beschleunigt, bis sie die Elektronen-Geschwindigkeit erreichen.
Die resultierende Impulsänderung ist als negative Frequenzänderung ($\eta<0$) sichtbar.

Die Korrelation und Gruppierung der Messdaten ergibt einen Frequenz-Mittelwert inklusive Streuung für jede gemessene Strahlposition (siehe \cref{fig:orbit_scan_data_correlated} im Anhang).
Aus der Umlauffrequenz wird dann der Impuls bestimmt (siehe \cref{momentum_frequency}), wobei die jeweils minimale Frequenz als Bezugspunkt~$f_0$ dient.
Die so ermittelte Impulsverteilung für die Protonen kann nach \cref{eq:cooling_relation} direkt in den (longitudinalen) Elektronen-Impuls umgerechnet werden.
Weiterhin folgt aus $p=mv$ die longitudinale Geschwindigkeitskomponente, wobei die Abhängigkeit der relativistische Masse vom Impuls berücksichtigt wurde.
\\

Während der Auswertung viel auf, dass der Ziel-Impuls der Protonen und die nominelle kinetische Energie der Elektronen nicht wie erwartet zusammenpassen.
Ein Protonen-Impuls von $p_p=\SI{2425}{\mega\eV\per\c}$ entspricht nach \cref{eq:gamma} einer kinetischen Energie der Elektronen von $E_{kin,e} = \SI{905.1}{\kilo\eV}$.
Die nominelle Energie, auf die die Elektronen beschleunigt werden, betrug zum Zeitpunkt der Messung jedoch \SI{907.9}{\kilo\eV}, was einem Protonen-Impuls von \SI{2430.5}{\mega\eV\per\c} entspricht.

Die beobachtete Differenz könnte durch einen Winkel von \ang{1.3} zwischen Elektronen- und Protonenstrahl erklärt werden.
Die dadurch verminderte Geschwindigkeitskomponente in Richtung des Protonenstrahls entspricht der gemessenen Abweichung.
Der Elektronen-Orbit wurde jedoch im Vorfeld auf den Protonen-Orbit mit einer Winkelabweichung von weniger als \ang{0.02} optimiert.
Die Orbit-Verschiebung erfolgt parallel und ohne nennenswerte Beeinflussung des Winkels, weshalb diese Erklärung ausgeschlossen werden kann.
Ein Energie-Verlust des Elektronenstrahls bis zur Kühlstrecke kann ebenfalls aufgrund des sehr guten Vakuums (besser \SI{2E-9}{\milli\bar}) ausgeschlossen werden.

Eine weitere Fehlerquelle besteht in der Annahme des Umfangs von \gls{cosy} als Orbit-Länge.
Da zum Zeitpunkt der Messung keine Orbit-Korrektur durchgeführt wurde, kann davon ausgegangen werden, dass die tatsächliche Flugbahn von der optimalen abweicht.
Ein gegenüber dem Umfang von \SI{183.47}{\meter} nach \cref{table:cosyparameter} um \SI{5.3}{\centi\meter} verlängerter Orbit liefert eine Erklärung für die beobachtete Abweichung.
Die gemessene Schottky-Frequenz von \SI{1.5239}{\mega\hertz} entspricht so dem für die Elektronen-Energie von \SI{907.9}{\kilo\eV} erwarteten Impuls.
Die mangels Orbit-Korrektur auftretende Abweichung kann durch ein geringes Nachfahren der Stromstärke der Dipol-Magnete nach der Beschleunigungsphase erklärt werden und ist somit plausibel\footnote{R. Stassen, Persönliche Kommunikation, November 2018}.
In der ionenoptischen Modell-Rechnung sind außerdem einige den Orbit beeinflussende Komponenten -- unter anderem die Magnete des Elektronenkühlers -- nicht berücksichtigt.
Daher wird im Folgenden statt der Orbit-Länge laut Modell die aus der Schottky-Messung errechneten Länge verwendet.
\\

\begin{figure}
	\centering
	\includegraphics[width=0.9\textwidth]{graphics/plots/profile/profiles_1x-1y_v_bin}
	\caption[Geschwindigkeits-Profil des Elektronenstrahls]{
		Longitudinale Elektronengeschwindigkeit~$v_\parallel$ im Profil.
		Links: Horizontales Profil, gemessen bei $y=\SI{3.6}{\milli\meter}$.
		Rechts: Vertikales Profil, gemessen bei $x=\SI{-1.5}{\milli\meter}$.
	}
	\label{fig:profile}
\end{figure}

\cref{fig:profile} zeigt ein Geschwindigkeits-Profil als Ergebnis einer der eingangs beschriebenen Messungen.
Weitere Diagramme der insgesamt drei Messreihen sind in \cref{app:profiles} beigefügt.
Die Profile weisen jeweils einen Punkt mit maximaler longitudinaler Geschwindigkeit auf, an dem die Elektronen ihre nominelle kinetische Energie besitzen (Referenzpunkt).
Horizontal nimmt die Geschwindigkeitskomponente~$v_\parallel$ von diesem Punkt ausgehend annähernd parabelförmig ab; im Abstand von \SI{2}{\milli\meter} vom Maximum um etwa \SI{0.01}{\permille}.
Vertikal ergibt sich ein flaches, leicht wellen-förmiges Profil, das erst am Rand abfällt, wo keine Kühlung mehr stattfindet.





\subsection{Theoretische Berechnung und Vergleich}

Eine Erklärung für die parabelförmig abnehmende longitudinale Geschwindigkeitskomponente kann in dem in \cref{larmor} eingeführten \emph{Galloping} gefunden werden.
Dabei kreisen die Elektronen um die longitudinalen Feldlinien in der Kühlsektion (Larmor-Rotation).
Durch die Spiralbewegung ist der Impuls-Vektor gegen das Feld verkippt, was effektiv eine Verringerung der longitudinalen Komponente bewirkt.
\\

Die proportionale Abhängigkeit des Larmor-Radius vom Abstand wird durch die Galloping-Rate~$\xi$ nach \cref{eq:galloping} beschrieben.
%\cref{fig:larmor_galloping} im Anhang zeigt die sich daraus ergebende kegelförmige Verteilung.
Die transversale Komponente~$v_\perp$ ist nach \cref{eq:larmor} proportional zum Larmor-Radius und zeigt somit die typische, kegelförmige Verteilung.
Die longitudinale Geschwindigkeitskomponente~$v_\parallel$ kann dann trigonometrisch ermittelt werden:
\begin{align}
	v_\parallel = \sqrt{v^2-v_\perp^2}
%	&&
%	\frac{v_\parallel}{v} = \sqrt{1-\left(\frac{v_\perp}{v}\right)^2}
\end{align}
Dabei ist $v$ die absolute Geschwindigkeit der Elektronen aufgrund ihrer kinetischen Energie.
Im \enquote{Galloping-Zentrum} $(x_0,y_0)$ liegt keine Larmor-Rotation vor, so dass hier $v_\parallel=v$ maximal ist.
\cref{fig:vlong} zeigt den resultierenden Rotationsparaboloiden um dieses Zentrum.
\\

\begin{figure}[b!]
	\centering
	\includegraphics[width=0.75\textwidth]{graphics/plots/profile/theo_vlong_1}
	\caption[Geschwindigkeitsverteilung als Folge von Galloping]{
		Verteilung der longitudinalen Geschwindigkeitskomponente im Elektronenstrahl als Folge von Galloping ($\xi=\SI{75}{\permille}$).
		In Grün sind die parabelförmigen Profile durch das Galloping-Zentrum eingezeichnet; in Lila zwei Profile durch den Koordinatenursprung.
	}
	\label{fig:vlong}
\end{figure}


Die theoretischen Geschwindigkeits-Parabel als Folge des Galloping sind den Messreihen in \cref{fig:orbit_scan_results_fit} im Anhang gegenübergestellt.
Die horizontalen Profile können durch Galloping in der Größenordnung von \SIrange{75}{90}{\permille} gut erklärt werden.
Das vertikale Verhalten weicht jedoch hiervon ab.

Die Galloping-Rate wurde auch mithilfe einer Software zur Messung der Larmor-Rotation ermittelt\footnote{A. Halama, Persönliche Kommunikation, Oktober 2018}.
Diese variiert das Magnetfeld des Hauptsolenoiden und berechnet aus den BPM-Messdaten den Larmor-Radius.
Durch Anregung einzelner Strahl-Sektoren kann der Larmor-Radius für einen Teil des Strahls ermittelt, und so aus zwei Radien auf eine Galloping-Rate geschlossen werden.
Die Parabeln aufgrund der so ermittelten Werte sind ebenfalls eingezeichnet.
Hierbei ist anzumerken, dass die Galloping-Messung erst am Folgetag stattfinden konnte, wobei sich das Kühlverhalten -- trotz identischer Einstellungen -- verändert haben könnte.

Es muss außerdem beachtet werden, dass die Messung der Geschwindigkeits-Profile über das Schottky-Spektrum immer eine Überlagerung aller mit dem Photonenstrahl wechselwirkender Elektronen darstellen.
Die Profile stellen daher eine Mittelung über die Breite des Protonen-Strahls ($2\sigma\approx\SI{2}{\milli\meter}$) dar, wobei sich eventuelle Dichteunterschiede negativ auswirken.




\subsection{Schlussfolgerung}

Die Messungen legen nahe, dass Galloping zwar eine Erklärung für den qualitativen Verlauf der Geschwindigkeits-Profile liefert, jedoch nicht als alleinige Ursache angesehen werden kann.
Vielmehr ist anzunehmen, dass auch Effekte höherer Ordnung, wie sie in \cref{larmor} beschrieben sind, auftreten.
Insbesondere die bislang vernachlässigte Bewegung 3. Ordnung muss genauer analysiert werden.
Da sie in den gebogenen Sektionen entsteht, tritt ihre Wirkung hauptsächlich in der radialen Ebene auf, jedoch kaum in axialer Richtung.
\cref{app:electron-motion} führt Berechnungen der Strahltemperatur im Sinne von Larmor-Rotation auf, die diese These stützen:
Dabei zeigt sich durch Kombination von Galloping- und Quadrupol-Bewegung die beobachtete Asymmetrie in der horizontalen und vertikalen Ebene.
Diese kann direkt in ein Geschwindigkeits-Profil übersetzt werden, bei dem -- je nach gewählten Parametern -- nur horizontal die parabelförmige Abnahme beobachtet werden kann.

Ein weiterer hier vernachlässigter Effekt ist der Einfluss der Raumladung, durch die innerhalb des Elektronenstrahls eine Potentialsenke entsteht.
Dadurch ist die Energie der Elektronen im Zentrum reduziert, was effektiv einer verringerten Geschwindigkeit entspricht.
Da keine der vorgenommenen Messungen eine nach oben geöffnete Parabel zeigt ist jedoch anzunehmen, dass der Effekt der Raumladungs-Senke gegenüber der dominierenden Larmor-Rotation vernachlässigt werden kann.
\\

Die gemessenen Profile zeigen deutlich, dass die Voraussetzungen für die Elektronenkühlung nicht optimal sind.
Die verringerte longitudinale Geschwindigkeitskomponente und der damit verbundene transversale Impuls beeinflussen die (transversale) Kühlung negativ.
Dadurch liegt die Equilibriums-Emittanz höher, denn die Larmor-Rotation entspricht einem zusätzlichen transversalen Heizterm.
Dies ist mit ein Grund für den gravierenden Unterschied in der Zeitskala der longitudinalen und transversalen Kühlung.
Es sind daher zwei Optimierungsempfehlungen zu nennen.

Erstens muss die Larmor-Oszillation stets auf ein Minimum reduziert werden, das heißt, dass insbesondere Effekte höherer Ordnung wie Galloping und Quadrupol-Bewegung kompensiert werden müssen.
Dies ist prinzipiell durch die passende Einstellung der Magnetfelder und Korrekturspulen entlang des Elektronen-Orbits möglich~\cite{Bryzgunov.2009}.
Aufgrund des großen Parameter-Raumes von insgesamt 111 individuell einstellbaren Stromstärken für die einzelnen Spulen kann eine effektive Optimierung jedoch nicht manuell, sondern nur automatisiert auf Modell-Basis erfolgen.

Zweitens sollte die Lage des Elektronen-Strahls relativ zum Protonen-Strahl so angepasst werden, dass die Protonen das \enquote{Galloping-Zentrum} treffen, wo die Larmor-Rotation am geringsten ausfällt.
Alternativ ist es aufgrund des Superpositionsprinzips der Larmor-Bewegungen verschiedener Ordnungen auch möglich, das Zentrum in den Strahlschwerpunkt zu legen, indem eine Larmor-Kompensation durchgeführt wird~\cite{Halama.2017}.
Diese kann die Galloping induzierte Larmor-Rotation jedoch nicht global verringern, sondern nur verschieben.

Daher wechselwirken gerade die Protonen eines noch nicht gekühlten (und damit größeren) Strahls mit weiter außenliegenden \enquote{heißen} Elektronen.
Wenn die Strahlgröße durch die Kühlung abnimmt verkleinert sich die Wechselwirkungszone, wobei die nahe am Galloping-Zentrum liegenden \enquote{kalten} Elektronen maßgeblich beteiligt sind.
So beginnt die transversale Kühlung erst langsam, wird jedoch im Verlauf der Phasenraumkompression schneller bis schließlich das Equilibrium erreicht wird.
Wenn der Protonenstrahl hingegen bereits einen verkleinerten Durchmesser aufweist -- beispielsweise durch die in \cref{comparison} beschriebene transversale Vor-Kühlung -- ist die Elektronenkühlung sowohl transversal als auch longitudinal wesentlich schneller.




