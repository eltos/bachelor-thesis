\section{Strahlparameter}



\subsection{Phasenraum}
\label{phasespace}

% Phasenraum, Verteilung der Teilchen, Phasenellipse, Strahlradius, Zusammenhang mit Twiss-Parameter → Emittanz
% Liouvill'scher Satz!

In einem Synchrotron wie \gls{cosy} werden viele Milliarden Teilchen beschleunigt und gespeichert.
Die physikalische Beschreibung eines solchen Teilchenstrahls weist Parallelen zur Optik auf, weshalb der Formalismus als \enquote{Ionenoptik} bezeichnet wird.
Die Ionenoptik bildet die Grundlage der modernen Beschleuniger-Physik.

Dabei wird jedes Einzelne der umlaufenden Teilchen durch seinen Ort und Impuls charakterisiert.
In transversaler Richtung verwendet man die Koordinaten $x$ und $y$ sowie die Divergenzen $x'$ und $y'$, welche durch die transversalen Impulskomponenten hervorgerufen werden.
Longitudinal bezieht man sich auf das mit Soll-Impuls~$p_0$ umlaufende Teilchen und gibt die relative Position als Phase~$\phi=\Delta s/s_{ges}$ sowie die relative Impulsabweichung~$\delta=\Delta p/p_0$ an.

\begin{figure}[!b]
	\centering
	\includegraphics[width=0.8\textwidth]{graphics/theory/phasespace}
	\caption{Phasenraum und Strahlprofil in der $x$-Ebene}
	\label{fig:phasespace}
\end{figure}

Diese sechs Größen ($x$, $x'$, $y$, $y'$, $\phi$ und $\delta$) bilden den sechsdimensionalen Phasenraum.
In dessen Ursprung liegt das zentrale Teilchen, welches mit Soll-Impuls~$p_0$ auf einer idealen Flugbahn umläuft.
Die Teilchen des realen Strahls sind meist gaußförmig um den Ursprung verteilt.
\cref{fig:phasespace} zeigt zur Veranschaulichung exemplarisch eine Dichteverteilung in der $x$-$x'$-Ebene.
Ergänzend ist das horizontale Strahlprofil als Projektion auf die $x$-Achse eingetragen.
\\

Nach dem Liouvill'sche Theorem ist \textcquote[341]{Hinterberger.2008}{\textelp{} die lokale Teilchendichte~$\rho$ längs der Bahn eines Teilchens im sechsdimensionalen Phasenraum invariant}.
Das bedeutet, dass sich die Teilchen zwar umverteilen können, sich das Volumen im Phasenraum dabei aber nicht ändert.
So führt eine Verkleinerung des Strahls in der $x$-Ebene beispielsweise zu einem Anstieg der Divergenz $x'$ (Fokussierung).

Wie wir später sehen werden gibt es nichtlineare Prozesse, für die das Theorem nicht gültig ist. Zu solchen Verfahren, mit denen eine Komprimierung des Phasenraum-Volumens erreicht werden kann, gehört beispielsweise auch die Elektronenkühlung.





\subsection{Emittanz und Strahlradius}
\label{emittance_beamsize}

Die Teilchen im $1\sigma$-Intervall der Gaußverteilung werden von der sogenannten Phasenellipse~$\matr{\sigma}$ eingeschlossen (siehe \cref{fig:phasespace})~\cite[151, 251\psq]{Hinterberger.2008}.
Diese den Strahl beschreibende Matrix muss im Einklang mit der Optik des Beschleunigers stehen, welche durch die sogenannten Twiss-Parametern $\alpha$, $\beta$ und $\gamma$ beschrieben wird und durch die Anordnung der verschiedenen Magnete (\emph{Lattice}) gegeben ist:
\begin{equation}
\label{eq:phaseellipsis}
\underbrace{
	\matr{\sigma} =
	\begin{pmatrix}
		\sigma_{11} & \sigma_{12} \\
		\sigma_{12} & \sigma_{22}
	\end{pmatrix}
}_{Strahl}
	=
	\epsilon_{x,y}
\underbrace{
	\begin{pmatrix}
		\beta_{x,y} & \alpha_{x,y} \\
		\alpha_{x,y} & \gamma_{x,y}
	\end{pmatrix}
}_{Optik}
\end{equation}
Der Faktor $\epsilon$ ist hierbei die Emittanz in \si{\milli\meter\milli\radian}.
Sie ist definiert als $\epsilon_{x,y}=\sqrt{\det{\matr{\sigma}}}$ und charakterisiert die Fläche der Phasenellipse.
Somit ist die Emittanz nach Liouville eine Erhaltungsgröße.

Die Emittanz ist eine charakteristische Kenngröße zur Beschreibung der Güte eines Teilchenstrahls und bestimmt, wie gut der Strahl fokussiert werden kann.
Anschaulich kann sie als Produkt aus Strahlgröße und Divergenz aufgefasst werden.
%Oft wird auch die vom Impuls unabhängige, normalisierte Emittanz $\epsilon^n=\epsilon\beta\gamma$ verwendet.
\\


Für den Strahlradius~$\sigma_{x,y}$ folgt mit \cref{eq:phaseellipsis} nach~\cite[156]{Hinterberger.2008}:
\begin{equation}
	\sigma_{x,y} = \sqrt{\sigma_{11}} = \sqrt{\epsilon_{x,y} \beta_{x,y}}
\end{equation}
Der Twiss-Parameter~$\beta_{x,y}$ wird auch als Beta-Funktion bezeichnet, da er Ortsabhängig ist.
Berücksichtigt man zusätzlich noch die Verbreiterung des Strahls durch Dispersion~$D^p_{x,y}$, so gilt~\cite[169]{Hinterberger.2008}:
\begin{equation}
\label{eq:beamsize}
	%\epsilon_{x,y} = \frac{\sigma_{x,y}^2 - \left(D^p_{x,y}\delta\right)^2}{\beta_{x,y}}
	\sigma_{x,y} = \sqrt{\epsilon_{x,y}\beta_{x,y} + \left(D^p_{x,y}\delta\right)^2}
\end{equation}


Umgekehrt kann so die Emittanz in beiden Ebenen aus einer Messung des jeweiligen Strahlradius (siehe \cref{ipm_profile}) bestimmt werden.
Dazu müssen lediglich die optischen Parameter $\beta_{x,y}$ und $D^p_{x,y}$ des Beschleunigers am Ort der Messung, sowie die Impulsbreite~$\delta$ bekannt sein.
Erstere sind durch die Anordnung der Magnete des Beschleunigers definiert, letzterer widmet sich der folgende Abschnitt.





\subsection{Umlauffrequenz und Impulsverteilung}
\label{momentum_frequency}

Der Zusammenhang zwischen relativistischem Impuls~$p$ und Teilchen"=Geschwindigkeit~$v$ ist gegeben durch:
\begin{equation}
\label{eq:momentum}
	p
	= \gamma m_0 v
	= \frac{m_0 v}{\sqrt{1-\left(\frac{v}{c}\right)^2}}
	= \frac{m_0 \beta c}{\sqrt{1-\beta^2}}
	= m_0 c ~ \sqrt{\gamma^2-1}
\end{equation}
Mit der Ruhemasse des Teilchens~$m_0$, dem Lorentzfaktor~$\gamma$ und der relativen Geschwindigkeit~$\beta=v/c$.
\\

Um die geladenen Teilchen im Beschleuniger auf einer geschlossenen Umlaufbahn zu halten werden Dipol-Magnete verwendet.
Diese erzeugen ein homogenes Magnetfeld~$B$, in dem die Ionen durch die Lorentzkraft~$F_L=q \vec{v}\times\vec{B}$ auf Kreisbahnen gelenkt werden.
Aus der Geschwindigkeit~$v$ und dem Umfang~$U$ der Umlaufbahn folgt dann die Umlauffrequenz:
\begin{equation}
	% v = \beta c = f U
	f = \frac{v}{U}
\end{equation}
Diese Gleichung gilt jedoch nur für das synchrone Teilchen ($p=p_0$).
Teilchen die entsprechend der Verteilung im Phasenraum einen größeren Impuls haben ($\delta > 0$) sind nach \cref{eq:momentum} schneller.
Dadurch erfahren sie aber eine größere Lorentzkraft und werden stärker abgelenkt.
Folglich laufen sie auf einer verkürzten Innenbahn um.
Diese Bahnlängenänderung wird durch den sogenannten Momentum Compaction Faktor~$\alpha_p$ beschrieben:
% Für den Fall eines konstanten Dipol-Magnetfeldes gilt:
\begin{equation}
\label{eq:alpha_p}
	%\left.\frac{\Delta U}{U_0}\right\rvert_{B=const.}
	\frac{\Delta U}{U_0}
	= \alpha_p \frac{\Delta p}{p_0}
\end{equation}

Je nachdem ob die Bahnlängenänderung oder die Geschwindigkeitsänderung überwiegt folgt eine negative oder positive Frequenzänderung.
Dieser Zusammenhang ist in linearer Näherung durch den Faktor~$\eta$ gegeben \cite[267]{Hinterberger.2008}:
\begin{equation}
\label{eq:dff_dpp}
	\frac{\Delta f}{f_0}
	= \eta \frac{\Delta p}{p_0}
\end{equation}
\begin{equation}
\label{eq:eta}
	\eta
	= \frac{1}{\gamma^2} - \alpha_p
	= \frac{1}{\gamma^2} - \frac{1}{\gamma_{tr}^2}
\end{equation}
Hierbei ist $\gamma$ der Lorentzfaktor des Teilchens und $\gamma_{tr}$ (\emph{Gamma Transition}) eine von der Optik des Beschleunigers abhängige Größe.
Im niederenergetischen Fall ($\gamma<\gamma_{tr}$) überwiegt die Geschwindigkeitsänderung, so dass sich mit steigendem Impuls auch die Umlauffrequenz erhöht ($\eta>0$).
Oberhalb von $\gamma_{tr}$ dominiert jedoch die Bahnlängenänderung und die Umlauffrequenz beginnt wieder zu sinken ($\eta<0$).
Für $\gamma\to\gamma_{tr}$ läuft $\eta\to0$ was zu Instabilität des umlaufenden Teilchenpaketes und schließlich zu dessen Verlust führt.



