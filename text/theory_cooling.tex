\section{Strahlkühlung}



\subsection{Allgemeines}
\label{cooling_general}

Der Begriff der Strahlkühlung geht auf das thermodynamische Analogon der Temperatur eines Gases zurück.
Dabei bedeutet \enquote{Kühlen} eine Verringerung der Teilchenbewegung im Schwerpunktsystem; Bei einem Teilchenstrahl also die Reduktion transversaler Impulskomponenten sowie longitudinaler Impulsabweichungen.

Ziel ist es, das von einem Teilchenstrahl eingenommene Phasenraum-Volumen zu verkleinern.
Auf diese Weise kann eine hohe Impuls- und Ortsauflösung erreicht werden, wie sie für viele wissenschaftliche Experimente nötig ist.
Zusätzlich können auch Energieverluste durch interne Targets ausgeglichen und so die Lebensdauer des Strahls signifikant gesteigert werden.
Ein weiteres Einsatzgebiet ist die Teilchen-Akkumulation durch abwechselnde Injektion und Phasenraum-Kompression, wie sie beispielsweise beim \gls{hesr} zum Einsatz kommt~\cite[S.~837]{Katayama.2010}.
\\

Eine gute Übersicht über die verschiedene Methoden der Strahlkühlung bietet \cite{Mohl.2005}. Diese sind im Wesentlichen:
\begin{itemize}
	\setlength{\parskip}{0pt}
	\item Stochastische Kühlung
	\item Elektronenkühlung
	\item Ionisations-Kühlung
	\item Strahlungs-Kühlung
	\item Laserkühlung
\end{itemize}

Das Verfahren der \textbf{Stochastischen Kühlung} wurde 1968 von Simon van der Meer beschrieben und 1975 erstmals experimentell durchgeführt~\cite[407]{CAS.2009.Caspers}.
Das Prinzip beruht auf der in \cref{schottky} eingeführten Diagnose mittels Pick-Up.
Damit wird die longitudinale und transversale Abweichung einer Stichprobe aufgenommen und ein entsprechender Korrekturimpuls durch einen \emph{Kicker} auf den Strahl gegeben.
Die Kompensation der Impulsabweichungen über viele Umläufe führt so zu einer Phasenraumkompression.
Die Regelungsstechnische Umsetzung und prinzipiell auch die Kühlung erfolgt dabei in allen drei Ebenen getrennt.
Begrenzt ist die stochastische Kühlung durch das elektronische Rauschen der Signal verarbeitenden Elektroniken einerseits und die Durchmischung in den Stichproben andererseits~\cites[356]{Hinterberger.2008}{Stockhorst.2016}.
\\

Das Prinzip der \textbf{Elektronenkühlung} wurde 1966 erstmals von G. I. Budker theoretisch beschrieben~\cite{Budker.1967} und in den Folgejahren auch experimentell am \gls{binp} bestätigt~\cite{Budker.1978}.
Die Idee der Elektronenkühlung ist es, dem \enquote{warmen} Ionenstrahl durch Wechselwirkung mit einem \enquote{kalten} Elektronenstrahl Energie zu entziehen und so den Kühleffekt in allen drei Ebenen hervorzurufen.
Eine detaillierte Beschreibung folgt in den nächsten Abschnitten.
\\

Ebenfalls in den 60er Jahren wurde die \textbf{Ionisations-Kühlung} unter anderem für Myonen-Beschleuniger entwickelt.
Hierbei wird dem Strahl beim Durchgang durch ein Medium bewusst Energie in allen drei Dimensionen entzogen~\cite[225]{Mohl.2005}.
Die anschließende Beschleunigung kompensiert nur die longitudinale Geschwindigkeitsabnahme, wodurch sich effektiv eine transversale Kühlung ergibt.
Ein ähnliches Verfahren, die sogenannte \textbf{Strahlungs-Kühlung} für Elektronen, wurde in den 50er Jahren vorgeschlagen; Sie beruht auf dem gerichteten Energieverlust durch Synchrotron-Strahlung.
\\

Die \textbf{Laserkühlung} ist ein erst in den 80er Jahren entwickeltes Verfahren, bei dem ein Laser eine im Schwerpunktsystem des Strahls stehende Welle erzeugt~\cite[731]{Bonderup.1995}.
Durch Absorption und spontane Emission können damit ganz bestimmte Ionen gekühlt werden.
Das Verfahren dient hautsächlich der longitudinalen Kühlung ist aber aus naheliegenden Gründen auf Protonenstrahlen nicht anwendbar.





\subsection{Elektronenkühlung}
\label{ecool}

Die Elektronenkühlung~\cite{Parkhomchuk.2000} \enquote{umgeht} das Liouvill'sche Theorem (\cref{phasespace}), indem der zu kühlende Strahl durch einen Elektronenstrahl gelenkt wird (\cref{fig:cooling}).
Die Konstanz des Phasenraumvolumens gilt dabei für das Gesamtsystem beider Teilchenstrahlen.
Auf diese Weise kann der Ionenstrahl gekühlt werden, während sich der Elektronenstrahl gleichermaßen aufheizt.
\\

Um eine möglichst effektive Elektronenkühlung zu realisieren bedarf es eines \enquote{kalten}, das heißt mono-energetischen Elektronenstrahls mit geringen transversalen Impulskomponenten.
Beide Strahlen müssen außerdem die gleiche Geschwindigkeit haben, so dass sie im Schwerpunktsystem ruhen.
Anschaulich kann dann das Analogon zweier sich mischender Gase herangezogen werden, bei dem durch Coulomb-Stöße Energie ausgetauscht wird, bis sich ein thermodynamisches Gleichgewicht einstellt~\cite{Hinterberger.2008}.
Da der Elektronenstrahl jedoch kontinuierlich neu generiert wird, kann seine Temperatur als konstant angesehen und bis zu dieser gekühlt werden.
\\

Zur Beschreibung eines relativistischen Teilchens kann der Lorentzfaktor herangezogen werden:
\begin{equation}
\label{eq:gamma}
	\gamma
	= \frac{1}{\sqrt{1-\left(\frac{v}{c}\right)^2}}
	= \frac{E}{E_0}
	= \frac{\sqrt{(p c)^2 + E_0^2}}{E_0}
	= \sqrt{\left(\frac{pc}{E_0}\right)^2+1}
\end{equation}
Hierbei ist $E_0=m_0 c^2$ die Ruheenergie, $E$ die Gesamtenergie und $p$ der Impuls des mit der Geschwindigkeit $v$ propagierenden Teilchens.
Bei gleicher Geschwindigkeit und somit gleichem Lorentzfaktor von Ionen und Elektronen folgt aus dieser Gleichung unmittelbar, dass auch die Verhältnisse $E/E_0$ sowie $p/E_0$ identisch sind.
Im Falle eines Protonenstrahls muss daher für das Verhältnis von Protonen- zu Elektronenenergie die folgende Beziehung gelten:
\begin{equation}
\label{eq:cooling_relation}
	\frac{E_p}{E_e}
	= \frac{p_p}{p_e}
	= \frac{E_{0,p}}{E_{0,e}}
	= \frac{m_p}{m_e}
	\approx 1836.153
\end{equation}
Damit ergibt sich beispielsweise für Protonen mit $p_p=\SI{2425}{\mega\eV\per\c}$ eine kinetische Energie der Elektronen von $E_{kin,e} = \SI{905.1}{\kilo\eV}$.
\\

Der Elektronenkühlung wirken Aufheizeffekte entgegen, so dass sich ein Gleichgewichtszustand (Equilibrium) einstellt.
Hier ist insbesondere die Streuung der Ionen untereinander, das \emph{Intra-Beam-Scattering}~\cite{Piwinski.1974,Sorensen.1987}, zu nennen.
Dieses wird umso größer, je stärker die Ionen im Phasenraum komprimiert werden, und begrenzt so die erreichbaren Emittanzen und Impulsbreiten.
Hinzu kommt das durch Restgasstreuung hervorgerufene kontinuierliche Emittanzwachstum~\cite[345]{Hinterberger.2008}.
Interne Targets stellen ebenfalls eine je nach Dichte erhebliche Streuquelle dar.
In der Vergangenheit wurde an \gls{cosy} außerdem ein weiterer, bisher nicht identifizierter Aufheizeffekt beobachtet\footnote{T. Katayama, Persönliche Kommunikation, Oktober 2018}, der die transversale Kühlung verlangsamt und die erreichbare Equilibriums-Emittanz heraufsetzt.

Auch die Eigenschaften des Elektronenstrahls selbst beeinflussen die Kühlwirkung erheblich.
So spielen Winkel und Lage des Elektronenstrahls relativ zum Protonenstrahl eine entscheidende Rolle.
Weitere Effekte wie Larmor-Rotation (\cref{larmor}) erhöhen die effektive transversale Elektronentemperatur stark und verringern dadurch die Kühlwirkung.

\begin{figure}
	\centering
	\includegraphics[width=0.9\textwidth]{graphics/cooling}
	\caption{Das Prinzip der Elektronenkühlung}
	\label{fig:cooling}
\end{figure}





\clearpage
\subsection{Der 2MeV Elektronekühler an COSY}

2013 wurde an COSY ein Hochenergie-Elektronenkühler in Betrieb genommen, der seit 2009 am \gls{binp} entwickelt worden war~\cite{kamerdzhiev.2015}.
\cref{table:coolerparameter} listet einige wichtige Designparameter des Kühlers auf.
Mit Energien bis zu \SI{2}{\mega\eV} ist dieser zur Kühlung von Protonen mit Impulsen bis zu \SI{4.5}{\giga\eV\per\c} ausgelegt.
Die Besonderheit ist dabei die magnetisierte Kühlung, hervorgerufen durch ein starkes Magnetfeld in der Kühlsektion, über die andere Hochenergie-Kühler wie beispielsweise der \SI{4.3}{\mega\eV} Kühler am Fermilab~\cite{Nagaitsev.2015} nicht verfügen.
\\

\begin{figure}[b!]
	\centering
	\includegraphics[width=0.9\textwidth]{graphics/cooler_labelled}
	\caption[Modell des 2MeV Elektronenkühlers]{
		Modell des 2MeV Elektronenkühlers.
		Zur Veranschaulichung wurden einige Teile ausgeblendet.
	}
	\label{fig:cooler}
\end{figure}


\cref{fig:cooler} zeigt den Kühler im Modell.
Die Führung des Elektronenstrahls ergibt sich aus der Einpassung in die bereits bestehende Infrastruktur von \gls{cosy}.

In der \textbf{Elektronen-Kanone} (\emph{Gun}) werden die Elektronen von der beheizten Kathodenoberfläche emittiert, wobei der Elektronen-Strom durch die Anodenspannung gesteuert wird.
Die anschließende \textbf{Beschleunigung} erfolgt mit 33 kaskadenartig angeordneten, bipolaren Hochspannungssektionen (\emph{Accelerating column}).
Diese befinden sich aus Gründen der Spannungsfestigkeit in einem mit $SF_6$ gefüllten Drucktank.

Mithilfe eines \textbf{Strahlführungssystems}, das im Wesentlichen aus Solenoiden, Toroiden und Dipol-Korrekturspulen besteht, werden die beschleunigten Elektronen zur Kühlsektion geführt.
Sie folgen dabei durchgehend einem longitudinalen Magnetfeld.
Entlang des Transportkanals sind insgesamt 53 einzeln ansteuerbare Korrekturspulen angebracht, um den Elektronen-Orbit bestmöglich optimieren zu können.
Insgesamt 12 \glspl{bpm} messen nicht nur die Lage des Elektronenstrahls, sondern in der Kühlstrecke auch die Position des Protonenstrahls, damit beide optimal aufeinander abgestimmt werden können.

In der \textbf{Kühlsektion} erfolgt die Ein- und Auskopplung in den Protonen-Strahl mittels zweier \ang{45} Toroide, deren Einfluss auf den Protonenstrahl vor und nach der Kühlstrecke durch Dipol-Magnete kompensiert wird.
Der Hauptsolenoid besteht aus 40 exakt justierten Einzelspulen und erzeugt ein starkes und sehr homogenes longitudinales Magnetfeld, das eine transversale Bewegung der Elektronen minimieren soll.
Daher spricht man auch von magnetisierter Kühlung.
% Dabei treten jedoch Effekte der Larmor-Rotation auf, welche im folgenden Abschnitt erörtert werden.

Nach der \textbf{Rückführung} in den Drucktank passieren die Elektronen erneut die Beschleunigungssektion in umgekehrter Richtung.
Die Abbremsung im gleichen Elektrischen Feld dient der Rekupperation.
Nur so ist es möglich, einen Elektronenstrom von bis zu \SI{3}{\ampere} bei \SI{2}{\mega\eV} (\SI{6}{\mega\watt}) dauerhaft aufrecht zu erhalten.
% Bei 0.61A und 908keV (550kW) liegt der tatsächliche Verbrauch [COSY:2MEV:CASCADE:IN:P] bei etwa 15kW (3%).
Bei zu hohen Verlustströmen oder gar einem Strahlverlust wird der Elektronenstrahl automatisch abgeschaltet um Überlastung zu verhindern.

Bevor die entschleunigten Elektronen im \textbf{Kollektor} gesammelt werden, passieren sie einen Wien-Filter.
Dieser verhindert, dass Elektronen in den Transportkanal zurückgelangen können.
Dies ist insbesondere für die beim Auftreffen auf den Kollektor sekundär emittierten Elektronen entscheidend.
%Der Elektronenstrom wird anschließend zurück zum Fillament geleitet, wo er erneut emittiert werden kann.

\begin{table}
	\centering
	\def\arraystretch{1.2}
	\begin{tabular}{|l|l|}
		\hline
		Kinetische Energie der Elektronen  & \SIrange[exponent-to-prefix]{25}{2E3}{\kilo\eV} \\ \hline
		Maximaler Strom                    & \SI{3}{\ampere}                                 \\ \hline
		Länge der Kühlsektion              & \SI{2.69}{\meter}                               \\ \hline
		Magnetfeld in der Kühlsektion      & bis \SI{0.2}{\tesla}                            \\ \hline
		Vakuum der Elektronenstrahlführung & \SIrange{E-9}{E-10}{\milli\bar}                 \\ \hline
	\end{tabular}
	\caption[Parameterliste des 2MeV Elekronenkühlers]{
		Designparameter des 2MeV Elekronenkühlers  (Daten nach \cite{kamerdzhiev.2015})
	}
	\label{table:coolerparameter}
\end{table}






\subsection{Larmor Rotation und Galloping bei der magnetisierten Kühlung}
\label{larmor}

Idealerweise folgen die Elektronen exakt den Feldlinien des Führungs-Magnetfeldes.
In der Praxis ist dies jedoch aus mehreren Gründen nicht gegeben~\cite{Bryzgunov.2009}:
\begin{itemize}
	\setlength{\parskip}{0pt}
	%\setlength{\itemsep}{1pt}
	\item Das Magnetfeld ist nicht ideal, sondern weißt eine gewisse Welligkeit auf.
	\item An den Übergangsstellen überlappen die Felder angrenzender Magnete.
	\item In den gekrümmten Sektionen werden die Elektronen mit einem konstanten Radius abgelenkt während das longitudinale Führungsfeld zentrisch verläuft.
	\item Die Feldstärke variiert an den Übergängen, insbesondere nach der Beschleunigungssektion, was zu einer Krümmung der Feldlinien führt.
\end{itemize}
All diese Effekte führen dazu, dass die Elektronenbewegung an vielen Stellen nicht exakt parallel zum Führungsfeld ausgerichtet ist.
Die infolge dessen auftretende senkrechte Geschwindigkeitskomponente $v_\perp$ erzeugt eine Lorentzkraft~$F_L = q v_\perp B$, die zur Larmor-Rotation um die longitudinalen Feldlinien führt. Überlagert mit der Propagation ergibt sich damit eine spiralförmige Bewegung.
Der Larmor-Radius der Rotation folgt aus $F_L = m\frac{v_\perp^2}{r_L}$ zu:
\begin{equation}
\label{eq:larmor}
	r_L = \frac{m v_\perp}{q B}
\end{equation}
Hierbei ist $m=\gamma m_e$ die Masse des relativistischen Elektrons, $q=e$ seine Ladung und $B$ die magnetische Flussdichte des Solenoidfeldes.
\\


Da Radius und Phase der Larmor-Rotation einzelner Elektronen im Strahl unterschiedlich sein können, ergeben sich für den Elektronenstrahl verschiedene Moden.
Vereinfacht lässt sich die Bewegung in folgende Ordnungen einteilen~\cite{Bryzgunov.2009}:
\begin{itemize}
	\setlength{\parskip}{0pt}
	%\setlength{\itemsep}{1pt}
	\item[0.] Verschiebung des Strahls (keine Rotation)
	\item[1.] Phasengleiche Rotation mit konstantem Radius.
	          Dabei rotiert der Strahl als Ganzes, während seine Form erhalten bleibt.
	\item[2.] Radial lineare Zunahme des Larmor-Radius mit azimutaler Phasenverschiebung.
	          Dies führt zu einer periodischen Änderung der Strahlgröße (\enquote{Pulsen}) entlang seiner Bewegungsrichtung, was auch als \textbf{Galloping}\footnotemark{} bezeichnet wird.
	\item[3.] Radial lineare Zunahme des Larmor-Radius mit inverser azimutaler Phasenverschiebung.
	          Der Strahl wird periodisch in einer Richtung gestaucht und in der dazu senkrechten gedehnt (Quadrupol-Bewegung).
	          Der Strahl \enquote{Wobbelt}.
\end{itemize}
\footnotetext{Anstelle der Bezeichnung \enquote{Galloping} ist in der Literatur auch der bedeutungsgleiche Begriff \enquote{Scalloping} zu finden.}%
\cref{fig:electron_motions} zeigt diese Bewegungsmuster.
Die tatsächliche Strahlbewegung ergibt sich daraus als Überlagerung, wobei die erste und zweite Ordnung dominieren.
\\

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{graphics/theory/electron_motion_1}
		\caption{1. Ordnung}
	\end{subfigure}
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{graphics/theory/electron_motion_2}
		\caption{2. Ordnung}
	\end{subfigure}
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{graphics/theory/electron_motion_3}
		\caption{3. Ordnung}
	\end{subfigure}
	\caption[Illustration der Elektronenbewegung im Strahlquerschnitt]{
		Illustration der Elektronenbewegung im Strahlquerschnitt.
		Die zeitliche Entwicklung ist durch die verblassende Schattierung angedeutet und zusätzlich ist die Strahl-Einhüllende jeweils zu Beginn und nach einer halben Periode eingezeichnet.
	}
	\label{fig:electron_motions}
\end{figure}



Die Abhängigkeit des Larmor-Radius entsteht hauptsächlich durch die beiden im Folgenden beschriebenen Effekte.

Zum einen ist hier die Zunahme der magnetischen Flussdichte am Übergang zwischen Beschleunigungssektion und der ersten gebogenen Sektion zu nennen.
Dabei werden die Feldlinien gestaucht beziehungsweise eingeschnürt, das heißt außenliegende krümmen sich nach innen.
Ein Elektron im Zentrum passiert den Übergang störungsfrei, weiter außen gelegene Elektronen werden jedoch durch die Krümmung -- wie eingangs beschrieben -- auf Spiralbahnen gelenkt.
Dabei steigt die Feldlinienkrümmung und somit die transversale Geschwindigkeitskomponente sowie der Larmor-Radius mit zunehmendem Abstand vom Zentrum an.

Der zweite maßgebende Effekt liegt in den gebogenen Sektionen selbst.
Die Ablenkung erfolgt hier mit einem transversalen Magnetfeld, welches die Elektronen mit einem konstanten Krümmungsradius umlenkt.
Das Führungsfeldes ist jedoch radial-symmetrisch, das heißt der Krümmungsradius steigt linear nach Außen an.
Auch hier gibt es einen idealen Orbit, bei dem die beiden Radien exakt übereinstimmen.
Ein weiter außen gelegenes Elektron wird jedoch auf eine Bahn mit immer geringer werdender Krümmung gezwungen, ein weiter innen liegendes Elektron folgt entsprechend einer zunehmend zu stark gekrümmten Bahn.
Somit ergibt sich auch hier eine Larmor-Rotation deren Radius mit Abstand vom idealen Orbit zunimmt.
Im Gegensatz zum erstgenannten Effekt ist hier jedoch nur der Abstand in radialer Richtung der Biegung relevant, nicht jedoch die axiale Lage.
Daher treten zusätzlich die genannten Effekte dritter Ordnung auf~\cite{Bryzgunov.2009}.
\\


Um die Abhängigkeit des Larmor-Radius von der Lage innerhalb des Strahls in linearer Näherung zu beschreiben kann eine \enquote{Galloping-Rate}~$\xi$ eingeführt werden\footnote{A. Halama, Persönliche Kommunikation, Dezember 2018}:
\begin{equation}
\label{eq:galloping}
	r_L
	= \xi~\sqrt{(x-x_0)^2 + (y-y_0)^2}
	% = \sqrt{\left(\xi_x (x-x_0)\right)^2 + \left(\xi_y (y-y_0)\right)^2}
\end{equation}
Hierbei ist $r_L$ der Larmor-Radius an der Stelle ($x$,$y$). Die Koordinate ($x_0$,$y_0$) gibt die Lage des idealen Orbits an, bei dem keine Larmor-Rotation auftritt.








