\section{Eta-Messung mit der B-Jump Methode}
\label{eta-measure}

Um einen Zusammenhang zwischen der Umlauffrequenz als Messgröße und dem Protonen"=Impuls herzustellen, muss zunächst der Faktor $\eta$ bestimmt werden (siehe \cref{momentum_frequency}).
Die computergestützte ionen-optische Modellrechnung \enquote{MAD}~\cite{MAD} ist prinzipiell in der Lage den Wert durch eine Simulation zu ermitteln.
Der berechnete Wert von $\eta=-0.0624$ weicht jedoch Erfahrungsgemäß\footnote{R. Stassen, Persönliche Kommunikation, Oktober 2018} stark vom realen Wert ab, so dass Eta an dieser Stelle durch eine Messung bestimmt werden soll.

Hierbei kommt die sogenannte \enquote{B-Jump} Methode zum Einsatz.
Dabei wird die magnetische Feldstärke der Dipol-Magnete variiert, so dass sich die Bahnlänge der umlaufenden Teilchen um den gleichen Betrag ändert, den eine Impulsänderung hervorrufen würde.
Da sich der Teilchen-Impuls aber tatsächlich nicht ändert, kann die Bahnlängenänderung wie folgt aus der Umlauffrequenz bestimmt werden.
\\

Die Bahnlänge~$U$ der umlaufenden Teilchen ergibt sich aus der Summe aller geraden Strecken~$U_\parallel$ sowie dem Umfang der durch die Dipol-Magnete mit Radius~$r=p/(qB)$ gebogenen Sektionen:
\begin{equation}
	U
	= U_\parallel + 2\pi r
	= U_\parallel + \frac{2\pi}{q}\frac{p}{B}
\end{equation}
Hierbei ist $p$ der Impuls der Teilchen, $q$ ihre Ladung und $B$ die magnetische Flussdichte der Dipol-Magnete.
Durch partielles Ableiten nach $p$ und $B$ folgt dann die relative Änderung der Bahnlänge zu:
\begin{equation}
	\frac{dU}{U}
	= \frac{\frac{2\pi}{q}\left(\frac{dp}{B}-\frac{p\,dB}{B^2}\right)}{U_\parallel + \frac{2\pi}{q}\frac{p}{B}}
	= \frac{1}{1+U_\parallel \frac{q}{2\pi}\frac{B}{p}} \left(\frac{dp}{p}-\frac{dB}{B}\right)
%	= \frac{1}{1+\frac{U_\parallel}{2\pi r}} \left(\frac{dp}{p}-\frac{dB}{B}\right)
\end{equation}
Der Vorfaktor entspricht dem in \cref{momentum_frequency} eingeführten Momentum Compaction Factor~$\alpha_p$.
Im normalen Betrieb ändert sich das Magnetfeld nach der Beschleunigung nicht mehr und wir erhalten direkt \cref{eq:alpha_p}.
Für die Messung hingegen ändern wir das Magnetfeld, wobei der Impuls konstant ist:
\begin{align}
\label{eq:duu}
	\left.\frac{dU}{U}\right\rvert_{B = const}
	= \alpha_p \frac{dp}{p}
&&
	\left.\frac{dU}{U}\right\rvert_{p = const}
	= -\alpha_p \frac{dB}{B}
\end{align}
Mit dem Impuls bleibt auch die Teilchen-Geschwindigkeit~$v = U f$ während der Messung unverändert.
Daher entspricht die relative Bahnlängenänderung direkt einer (negativen) relativen Frequenzänderung.
Aus \cref{eq:duu} folgt dann:
\begin{equation}
\label{eq:alpha_p-measure}
	\alpha_p
	= -\frac{dU/U}{dB/B}
	= \frac{df/f}{dB/B}
\end{equation}
\\





% Data
% http://elog.cc.kfa-juelich.de:22375/Cosy+Operating/5227
\begin{table}
	\centering
	\def\arraystretch{1.1}
	\begin{tabular}{SSS}
		\multicolumn{1}{c}{$\Delta B / B$ in \si{\permille}}
		&\multicolumn{1}{c}{$f$ in $\si{\mega\hertz}$}
		&\multicolumn{1}{c}{$\Delta f / f_0$ in \si{\permille}}
		\\\hline
		-1.0  & 2437.86 & -0.159951 \\
		-0.5  & 2438.05 & -0.082026 \\
		0.0  & 2438.25 &  0        \\
		0.5  & 2438.46 &  0.086127 \\
		1.0  & 2438.66 &  0.168153
	\end{tabular}
	\caption[Messwerte zur Eta-Bestimmung nach der B-Jump Methode]{
		Messwerte zur Eta-Bestimmung nach der B-Jump Methode.
		Es wurde die 1600. Harmonische der Umlauffrequenz gemessen.
	}
	\label{table:b-jump}
\end{table}

Die Umlauffrequenz wurde für verschiedene Magnetfeldänderungen mithilfe der in \cref{schottky-ioc} beschriebenen Schottky-Messung ermittelt.
Die Messwerte sind in \cref{table:b-jump} dargestellt.

Aus der absoluten Frequenz ohne B-Jump kann zunächst der Lorenzfaktor $\gamma$ bestimmt werden.
Mit dem Umfang~$U$ von COSY nach \cref{table:cosyparameter} und $f_0 = \SI{2438.25}{\mega\hertz}/1600$ folgt mit $v = U f$:
\begin{equation*}
	\gamma
	= \frac{1}{\sqrt{1-\beta^2}}
	= \frac{1}{\sqrt{1-\left(\frac{U f_0}{c}\right)^2}}
	= 2.771
\end{equation*}
\\

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{graphics/plots/eta_measurement}
	\caption[Messdaten und lineare Regression der B-Jump Messung]{
		Messdaten und lineare Regression der B-Jump Messung.
		Aufgetragen ist die relative Änderung der Umlauffrequenz in Abhängigkeit der relativen Änderung der magnetischen Flussdichte der Dipol-Magnete (B-Jump).
	}
	\label{fig:eta-plot}
\end{figure}

In \cref{fig:eta-plot} wurden die Messwerte aufgetragen und eine lineare Regression durchgeführt.
Aus dieser folgt $\alpha_p$ nach \cref{eq:alpha_p-measure} direkt als Steigung:
\begin{equation}
	\alpha_p
%	= \frac{df/f}{dB/B}
	= \SI{0.1649 \pm 0.0016}{}
\end{equation}
Aus $\gamma$ und $\alpha_p$ kann mit \cref{eq:eta} schließlich der gesuchte Eta-Faktor berechnet werden:
\begin{equation}
\underline{\underline{
		\eta =
		\SI{-0.0346 \pm 0.0016}{}
		%   -0.0624  from MAD-X simulation
}}
\end{equation}







